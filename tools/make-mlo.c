#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

typedef unsigned u32;

_Static_assert(sizeof(u32) == 4, "Bad size for u32");

typedef struct Source
{
	char *contents;
	size_t length;
} Source;

static Source read_binary_source(const char *path);
static int write_mlo(const char *path, Source data, u32 load_address);

int main(int argc, char **argv)
{
	if(argc < 3)
		return 1;
	const char *src_path = argv[1];
	const char *dest_path = argv[2];

	Source src = read_binary_source(src_path);
	if(!src.contents)
	{
		fprintf(stderr, "Failed to read source `%s`\n", src_path);
		return 1;
	}

	int ret_code = write_mlo(dest_path, src, 0x402f0400);
	if(ret_code)
		fprintf(stderr, "Failed to write MLO\n");
	free(src.contents);
	return ret_code;
}

static Source read_binary_source(const char *path)
{
	FILE *fp = fopen(path, "rb");
	if(!fp)
	{
		return (Source) {
			.contents = 0
		};
	}
	fseek(fp, 0, SEEK_END);
	size_t length = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	char *contents = malloc(length);
	if(!contents)
	{
		fclose(fp);
		return (Source) {
			.contents = 0
		};
	}
	fread(contents, length, 1, fp);
	fclose(fp);
	return (Source) {
		.contents = contents,
		.length = length
	};
}

static int write_mlo(const char *path, Source data, u32 load_address)
{
	FILE *fp = fopen(path, "wb");
	if(!fp)
		return 1;

	u32 header[2] = {
		data.length,
		load_address
	};

	fwrite(&header, sizeof(header), 1, fp);
	fwrite(data.contents, data.length, 1, fp);
	fclose(fp);
	return 0;
}
