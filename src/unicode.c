#include "unicode.h"
#include "string.h"

UnicodeConversionResult unicode_utf8_to_utf16(Str src)
{
	u32 estimated_length = src.length * 2;
	StringResult str_res = String_new(estimated_length);
	if(str_res.status == STRING_OUT_OF_MEMORY)
		return (UnicodeConversionResult) { .status = UNICODE_OUT_OF_MEMORY };
	String result = str_res.result;
	u8 position_in_char = -1;
	u16 character = 0;
	for(u32 pos = 0; pos < src.length; pos++)
	{
		u8 ch = src.pointer[pos];
		if(ch < 0x80)	// ASCII
		{
			character = ch;
			Str ch = {
				.pointer = (void*)&character,
				.length = 2
			};
			String_append(&result, ch);
		}
		else if((ch & 0b11000000) == 0x80)	// in the character
		{
			if(position_in_char > 6)
			{
				String_drop(&result);
				return (UnicodeConversionResult) { .status = UNICODE_BAD_ENCODING };
			}
			u8 value = ch & 0b00111111;
			character |= (u16)value << position_in_char;
			if(position_in_char == 0)
			{
				character = ch;
				Str ch = {
					.pointer = (void*)&character,
					.length = 2
				};
				String_append(&result, ch);
				position_in_char = -1;
			}
			else
				position_in_char -= 6;
		}
		else if((ch & 0b11100000) == 0b11000000 && position_in_char == 255)	// 2 bytes
		{
			character = (ch & 0b00011111) << 6;
			position_in_char = 0;
		}
		else if((ch & 0b11110000) == 0b11100000 && position_in_char == 255)	// 3 bytes
		{
			character = (ch & 0b00001111) << 12;
			position_in_char = 6;
		}
		else if((ch & 0b11111000) == 0b11110000 && position_in_char == 255)	// 4 bytes
		{
			String_drop(&result);
			return (UnicodeConversionResult) { .status = UNICODE_CHAR_TOO_LONG };
		}
		else
		{
			String_drop(&result);
			return (UnicodeConversionResult) { .status = UNICODE_BAD_ENCODING };
		}
	}
	return (UnicodeConversionResult) {
		.status = UNICODE_OK,
		.result = result
	};
}

u32 unicode_utf16_strlen(char *str)
{
	u32 result = 0;
	while(str[result * 2] || str[result * 2 + 1])
		result++;
	return result;
}
