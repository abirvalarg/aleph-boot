#pragma once
#include "types.h"

// Just a pointer to a string with length
typedef struct Str
{
	char *pointer;
	u32 length;
} Str;

// A container that owns a string
typedef struct String
{
	char *contents;
	u32 length;
	u32 capacity;
} String;

typedef enum StringStatus
{
	STRING_OK,
	STRING_OUT_OF_MEMORY
} StringStatus;

typedef struct StringResult
{
	String result;
	StringStatus status;
} StringResult;

Str Str_from_cstr(char *contents);
int Str_contains(Str str, char ch);

StringResult String_new(u32 capacity);
void String_drop(String *string);
StringStatus String_push(String *string, char ch);
StringStatus String_append(String *string, Str other);
