#pragma once
#include "types.h"

typedef volatile u32 ControlRegs;

typedef enum CrystalClockFreq
{
	CR_CLOCK_FREQ_19_2 = 0b00,
	CR_CLOCK_FREQ_24 = 0b01,
	CR_CLOCK_FREQ_25 = 0b10,
	CR_CLOCK_FREQ_26 = 0b11,
} CrystalClockFreq;

typedef enum CPUModel
{
	CPU_UNKNOWN,
	CPU_AM3351,
	CPU_AM3352,
	CPU_AM3354,
	CPU_AM3356,
	CPU_AM3357,
	CPU_AM3358,
	CPU_AM3359
} CPUModel;

typedef enum SiliconRevision
{
	SIL_REV_10 = 0b0,
	SIL_REV_20 = 0b1,
	SIL_REV_21 = 0b10
} SiliconRevision;

typedef enum ControlPin
{
	CONTROL_PIN_GPMC_AD0 = 0x800 / 4,	// MMC1_DAT0
	CONTROL_PIN_GPMC_AD1 = 0x804 / 4,	// MMC1_DAT1
	CONTROL_PIN_GPMC_AD2 = 0x808 / 4,	// MMC1_DAT2
	CONTROL_PIN_GPMC_AD3 = 0x80c / 4,	// MMC1_DAT3
	CONTROL_PIN_GPMC_AD4 = 0x810 / 4,	// MMC1_DAT4
	CONTROL_PIN_GPMC_AD5 = 0x814 / 4,	// MMC1_DAT5
	CONTROL_PIN_GPMC_AD6 = 0x818 / 4,	// MMC1_DAT6
	CONTROL_PIN_GPMC_AD7 = 0x81c / 4,	// MMC1_DAT7
	CONTROL_PIN_GPMC_A4 = 0x850 / 4,	// eMMC_RST (GPIO)
	CONTROL_PIN_GPMC_A5 = 0x854 / 4,	// USR0 (GPIO)
	CONTROL_PIN_GPMC_A6 = 0x858 / 4,	// USR1 (GPIO)
	CONTROL_PIN_GPMC_A7 = 0x85c / 4,	// USR2 (GPIO)
	CONTROL_PIN_GPMC_A8 = 0x860 / 4,	// USR3 (GPIO)
	CONTROL_PIN_GPMC_CSN1 = 0x880 / 4,	// MMC1_CLK
	CONTROL_PIN_GPMC_CSN2 = 0x884 / 4,	// MMC1_CMD
	CONTROL_PIN_MMC0_DAT3 = 0x8f0 / 4,
	CONTROL_PIN_MMC0_DAT2 = 0x8f4 / 4,
	CONTROL_PIN_MMC0_DAT1 = 0x8f8 / 4,
	CONTROL_PIN_MMC0_DAT0 = 0x8fc / 4,
	CONTROL_PIN_MMC0_CLK = 0x900 / 4,
	CONTROL_PIN_MMC0_CMD = 0x904 / 4,
	CONTROL_PIN_MII1_TXD1 = 0x924 / 4,
	CONTROL_PIN_MDIO = 0x948 / 4,
	CONTROL_PIN_SPI0_CS1 = 0x960 / 4,	// MMC0_SDCD
	CONTROL_PIN_UART0_RXD = 0x970 / 4,
	CONTROL_PIN_UART0_TXD = 0x974 / 4,
} ControlPin;

extern const char *const cpu_names[8];
extern const char *const revision_names[3];

#define CONTROL_MODE_PULL_ENABLE 0
#define CONTROL_MODE_PULL_DISABLE (1 << 3)
#define CONTROL_MODE_PULL_DOWN 0
#define CONTROL_MODE_PULL_UP (1 << 4)
#define CONTROL_MODE_INPUT_DISABLE 0
#define CONTROL_MODE_INPUT_ENABLE (1 << 5)
#define CONTROL_MODE_SLEW_FAST 0
#define CONTROL_MODE_SLEW_SLOW (1 << 6)

CrystalClockFreq control_crystap_clock_freq(void);
u8 control_boot_config(void);
CPUModel control_cpu_model(void);
SiliconRevision control_silicon_revision(void);
void control_config_vtp(void);
void control_config_ddr(void);

/**
 * Set mode of the pin in CONTROL module
 * @param pin is a target pin selected from `ControlPin` enum
 * @param mode is a bitwise OR of the function mux value (0-7) and other options
 * (pull enable, pull up or down, input enable and slew rate)
 */
void control_pin_mode(ControlPin pin, u32 mode);

#define CONTROL_STATUS (0x40 / 4)
#define CONTROL_EMIF_SDRAM_CONFIG (0x110 / 4)
#define CONTROL_DEVICE_ID (0x600 / 4)
#define CONTROL_DEV_FEATURE (0x604 / 4)
#define CONTROL_VTP_CTRL (0xe0c / 4)
#define CONTROL_DDR_CKE_CTRL (0x131c / 4)
#define CONTROL_DDR_CMD0_IOCTRL (0x1404 / 4)
#define CONTROL_DDR_CMD1_IOCTRL (0x1408 / 4)
#define CONTROL_DDR_CMD2_IOCTRL (0x140c / 4)
#define CONTROL_DDR_DATA1_IOCTRL (0x1444 / 4)

#define CONTROL ((ControlRegs*)0x44E10000)
