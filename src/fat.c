#include "fat.h"
#include "heap.h"
#include "mbr.h"
#include "unicode.h"
#include "mem.h"

#include "uart.h"

typedef struct ShortNameResult
{
	u8 name[11];
	u8 is_short_name;
} ShortNameResult;

typedef struct ResourceInfo
{
	FatStatus status;
	u32 size;
	u32 first_cluster;
	u8 attributes;
} ResourceInfo;

typedef struct ShortNameEntry
{
	u8 name[11];
	u8 attributes;
	u8 nt_reserved;
	u8 create_time_tenth;
	u16 create_time;
	u16 create_date;
	u16 last_access_date;
	u16 first_cluster_high;
	u16 write_time;
	u16 write_date;
	u16 first_cluster_low;
	u32 file_size;
} ShortNameEntry;

typedef struct LongNameEntry
{
	u8 order;
	u8 name1[10];
	u8 attr;
	u8 type;
	u8 checksum;
	u8 name2[12];
	u16 first_cluster_low;
	u8 name3[4];
} LongNameEntry;

static char forbidden_short_name_char_codes[] = {
	0x22, 0x2a, 0x2b, 0x2c, 0x2e, 0x2f, 0x3a, 0x3b,
	0x3c, 0x3d, 0x3e, 0x3f, 0x5b, 0x5c, 0x5d, 0x7c
};
static Str forbidden_short_name_chars = {
	.pointer = forbidden_short_name_char_codes,
	.length = 16
};

static FatStatus init_old_fat(Fat *fat, const u8 *buffer);
static FatStatus init_fat32(Fat *fat, const u8 *buffer);
static ShortNameResult try_convert_to_short_name(Str name);
static ResourceInfo find_short_name_entry(FatDir *dir, u8 *name);
static ResourceInfo find_long_name_entry(FatDir *dir, Str name);
static FatStatus read_cluster(Fat *fat, u32 cluster_num, u8 *buffer);
static u32 last_fat_cluster(Fat *fat);

FatStatus Fat_init(Fat *fat, MbrPartition partition)
{
	fat->partition = partition;
	u8 *buffer = malloc(512);
	if(!buffer)
		return FAT_OUT_OF_MEMORY;
	MbrStatus mbr_st = MbrPartition_read_sector(partition, 0, buffer);
	if(mbr_st != MBR_OK)
	{
		free(buffer);
		return FAT_MBR_ERROR;
	}

	u16 num_root_entries = buffer[17] | buffer[18] << 8;

	FatStatus st;
	if(num_root_entries == 0)	// fat32
		st = init_fat32(fat, buffer);
	else	// fat16 or fat12
		st = init_old_fat(fat, buffer);

	if(st == FAT_OK)
	{
		fat->fat_sector_buffer = buffer;
		fat->fat_sector_is_valid = 0;
	}
	else
		free(buffer);

	return st;
}

void Fat_deinit(Fat *fat)
{
	free(fat->fat_sector_buffer);
}

FatDir Fat_open_root_dir(Fat *fat)
{
	if(fat->kind == FAT32)
	{
		return (FatDir) {
			.fat = fat,
			.first_cluster = fat->fat32_info.root_dir_first_cluster,
			.old_fat_root_dir = 0
		};
	}
	else
	{
		return (FatDir) {
			.fat = fat,
			.old_fat_root_dir = 1
		};
	}
}

FatFileResult FatDir_open(FatDir *dir, Str name)
{
	ShortNameResult short_name_res = try_convert_to_short_name(name);

	ResourceInfo info;
	if(short_name_res.is_short_name)
		info = find_short_name_entry(dir, short_name_res.name);
	else
		info = find_long_name_entry(dir, name);

	if(info.status != FAT_OK)
		return (FatFileResult) { .status = info.status };

	if(info.attributes & FAT_ATTR_DIRECTORY)
		return (FatFileResult) { .status = FAT_WRONG_OBJECT_KIND };

	FatFile *fp = malloc(sizeof(FatFile));
	if(!fp)
		return (FatFileResult) { .status = FAT_OUT_OF_MEMORY };

	u8 *read_buffer = malloc(512 * dir->fat->sectors_per_cluster);
	if(!read_buffer)
	{
		free(fp);
		return (FatFileResult) { .status = FAT_OUT_OF_MEMORY };
	}

	*fp = (FatFile) {
		.fat = dir->fat,
		.read_buffer = read_buffer,
		.buffer_size = 0,
		.buffer_start = 0,
		.next_cluster = info.first_cluster,
		.left_on_disk = info.size
	};

	return (FatFileResult) {
		.result = fp,
		.status = FAT_OK
	};
}

FatDirResult FatDir_open_dir(FatDir *dir, Str name)
{
	ShortNameResult short_name_res = try_convert_to_short_name(name);

	ResourceInfo info;
	if(short_name_res.is_short_name)
		info = find_short_name_entry(dir, short_name_res.name);
	else
		info = find_long_name_entry(dir, name);

	if(info.status != FAT_OK)
		return (FatDirResult) { .status = info.status };

	if(!(info.attributes & FAT_ATTR_DIRECTORY))
		return (FatDirResult) { .status = FAT_WRONG_OBJECT_KIND };

	return (FatDirResult) {
		.status = FAT_OK,
		.result = {
			.fat = dir->fat,
			.first_cluster = info.first_cluster,
			.old_fat_root_dir = 0
		}
	};
}

void FatFile_close(FatFile *fp)
{
	free(fp->read_buffer);
	free(fp);
}

FatFileReadResult FatFile_read(FatFile *fp, u8 *buffer, u32 length)
{
	if(fp->buffer_size == 0)
	{
		const u32 cluster_size = 512 * fp->fat->sectors_per_cluster;
		u32 cluster_entries_per_fat_sector = 512 / (fp->fat->kind == FAT16 ? 2 : 4);

		if(fp->left_on_disk == 0 || fp->next_cluster == last_fat_cluster(fp->fat))
		{
			return (FatFileReadResult) {
				.length = 0,
				.status = FAT_OK
			};
		}
		FatStatus read_st = read_cluster(fp->fat, fp->next_cluster, fp->read_buffer);
		if(read_st != FAT_OK)
		{
			return (FatFileReadResult) {
				.status = read_st
			};
		}
		fp->buffer_start = 0;
		fp->buffer_size = fp->left_on_disk < cluster_size ? fp->left_on_disk : cluster_size;
		fp->left_on_disk -= fp->buffer_size;
		u32 next_cluster_location_sector = fp->fat->num_reserved_sectors +
			fp->next_cluster / cluster_entries_per_fat_sector;
		if(!fp->fat->fat_sector_is_valid
				|| fp->fat->sector_in_fat_buffer != next_cluster_location_sector)
		{
			MbrStatus fat_read_st = MbrPartition_read_sector(
					fp->fat->partition,
					next_cluster_location_sector,
					fp->fat->fat_sector_buffer
			);
			if(fat_read_st != MBR_OK)
				return (FatFileReadResult) { .status = FAT_MBR_ERROR };
			fp->fat->fat_sector_is_valid = 1;
			fp->fat->sector_in_fat_buffer = next_cluster_location_sector;
		}
		u32 next_cluster_location_index = fp->next_cluster % cluster_entries_per_fat_sector;
		if(fp->fat->kind == FAT16)
			fp->next_cluster = ((u16*)fp->fat->fat_sector_buffer)[next_cluster_location_index];
		else
			fp->next_cluster = ((u32*)fp->fat->fat_sector_buffer)[next_cluster_location_index];
	}

	u32 bytes_to_copy = length < fp->buffer_size ? length : fp->buffer_size;
	for(u32 pos = 0; pos < bytes_to_copy; pos++)
		buffer[pos] = fp->read_buffer[fp->buffer_start + pos];
	fp->buffer_start += bytes_to_copy;
	fp->buffer_size -= bytes_to_copy;
	return (FatFileReadResult) {
		.length = bytes_to_copy,
		.status = FAT_OK
	};
}

static FatStatus init_old_fat(Fat *fat, const u8 *buffer)
{
	u32 fat_size = buffer[22] | buffer[23] << 8;
	u32 reserved_sectors = buffer[14] | buffer[15] << 8;
	u8 num_fats = buffer[16];
	u16 num_root_entries = buffer[17] | buffer[18] << 8;
	u16 total_sectors16 = buffer[19] | buffer[20] << 8;
	u32 total_sectors = total_sectors16 ?
		total_sectors16
		: buffer[32] | buffer[33] << 8 | buffer[34] << 16 | buffer[35] << 24;
	u32 num_data_sectors = total_sectors - reserved_sectors - (num_fats * fat_size)
		- (num_root_entries * 32 / 512);
	u8 sectors_per_cluster = buffer[13];
	u32 num_clusters = num_data_sectors / sectors_per_cluster;

	fat->num_clusters = num_clusters;
	fat->old_fat_info.num_root_entries = num_root_entries;
	fat->old_fat_info.fat_size = fat_size;
	fat->num_reserved_sectors = reserved_sectors;
	fat->sectors_per_cluster = sectors_per_cluster;
	fat->num_fats = num_fats;

	if(num_clusters < 4085)
		return FAT_NOT_IMPLEMENTED;
	else
		fat->kind = FAT16;

	return FAT_OK;
}

static FatStatus init_fat32(Fat *fat, const u8 *buffer)
{
	fat->kind = FAT32;

	u32 fat_size = buffer[36] | buffer[37] << 8 | buffer[38] << 16 | buffer[39] << 24;
	u8 num_fats = buffer[16];
	u32 reserved_sectors = buffer[14] | buffer[15] << 8;
	u32 total_sectors = buffer[32] | buffer[33] << 8 | buffer[34] << 16 | buffer[35] << 24;
	u8 sectors_per_cluster = buffer[13];
	u32 root_dir_first_cluster = buffer[44] | buffer[45] << 8 | buffer[46] << 16 | buffer[47] << 24;

	u32 num_data_sectors = total_sectors - reserved_sectors - (num_fats * fat_size);
	u32 num_clusters = num_data_sectors / sectors_per_cluster;

	fat->num_clusters = num_clusters;
	fat->fat32_info.fat_size = fat_size;
	fat->fat32_info.root_dir_first_cluster = root_dir_first_cluster;
	fat->num_reserved_sectors = reserved_sectors;
	fat->sectors_per_cluster = sectors_per_cluster;
	fat->num_fats = num_fats;

	return FAT_OK;
}

static ShortNameResult try_convert_to_short_name(Str name)
{
	if(name.length > 12)
		return (ShortNameResult) { .is_short_name = 0 };
	ShortNameResult result = { .is_short_name = 1 };
	u8 fill_with_spaces = 0;
	unsigned name_pos = 0;
	for(unsigned pos = 0; pos < 8; pos++)
	{
		if(fill_with_spaces)
			result.name[pos] = ' ';
		else
		{
			if(name_pos == name.length)
			{
				fill_with_spaces = 1;
				result.name[pos] = ' ';
			}
			else
			{
				char ch = name.pointer[name_pos++];
				if(ch == '.')
				{
					fill_with_spaces = 1;
					result.name[pos] = ' ';
				}
				else if(ch < 0x20 || (ch > 'a' && ch < 'z')
						|| Str_contains(forbidden_short_name_chars, ch))
					return (ShortNameResult) { .is_short_name = 0 };
				else
					result.name[pos] = ch;
			}
		}
	}
	if(!(fill_with_spaces || name.pointer[name_pos] == '.' || name_pos == name.length))
		return (ShortNameResult) { .is_short_name = 0 };
	if(name.pointer[name_pos] == '.')
		name_pos++;
	fill_with_spaces = 0;
	for(unsigned pos = 8; pos < 11; pos++)
	{
		if(fill_with_spaces)
			result.name[pos] = ' ';
		else
		{
			if(name_pos == name.length)
			{
				fill_with_spaces = 1;
				result.name[pos] = ' ';
			}
			else
			{
				char ch = name.pointer[name_pos++];
				if(ch < 0x20 || (ch > 'a' && ch < 'z')
						|| Str_contains(forbidden_short_name_chars, ch))
					return (ShortNameResult) { .is_short_name = 0 };
				else
					result.name[pos] = ch;
			}
		}
	}
	if(name_pos == name.length)
		return result;
	else
		return (ShortNameResult) { .is_short_name = 0 };
}

static ResourceInfo find_short_name_entry(FatDir *dir, u8 *name)
{
	if(dir->old_fat_root_dir)
	{
		u16 num_entries = dir->fat->old_fat_info.num_root_entries;
		u32 root_dir_size = (u32)num_entries * 32;
		u32 num_root_dir_sectors = root_dir_size / 512 + (root_dir_size % 512 ? 1 : 0);
		u32 root_buffer_size = num_root_dir_sectors * 512;
		u32 first_dir_sector = dir->fat->num_reserved_sectors +
			dir->fat->old_fat_info.fat_size * dir->fat->num_fats;

		u8 *root_dir_buffer = malloc(root_buffer_size);
		if(!root_dir_buffer)
			return (ResourceInfo) { .status = FAT_OUT_OF_MEMORY };

		for(u32 sector = 0; sector < num_root_dir_sectors; sector++)
		{
			MbrStatus st = MbrPartition_read_sector(
					dir->fat->partition,
					first_dir_sector + sector,
					root_dir_buffer + sector * 512
				);
			if(st != MBR_OK)
			{
				free(root_dir_buffer);
				return (ResourceInfo) { .status = FAT_MBR_ERROR };
			}
		}

		for(u32 entry = 0; entry < num_entries; entry++)
		{
			ShortNameEntry *entry_info = (ShortNameEntry*)(root_dir_buffer + entry * 32);
			if(entry_info->name[0] == 0)
			{
				free(root_dir_buffer);
				return (ResourceInfo) { .status = FAT_NO_OBJECT };
			}
			u8 matches = 1;
			for(unsigned pos = 0; pos < 11; pos++)
			{
				if(entry_info->name[pos] != name[pos])
				{
					matches = 0;
					break;
				}
			}

			if(matches)
			{
				ResourceInfo result = {
					.status = FAT_OK,
					.attributes = entry_info->attributes,
					.first_cluster = (u32)entry_info->first_cluster_high << 16
						| (u32)entry_info->first_cluster_low,
					.size = entry_info->file_size
				};
				free(root_dir_buffer);
				return result;
			}
		}

		free(root_dir_buffer);
		return (ResourceInfo) { .status = FAT_NO_OBJECT };
	}
	else	// subdirectory or FAT32 root dir
	{
		u32 cluster_size = 512 * dir->fat->sectors_per_cluster;
		u32 entries_per_cluster = cluster_size / 32;
		u32 next_cluster = dir->first_cluster & 0xfffffff;
		u32 last_cluster = last_fat_cluster(dir->fat);
		u32 cluster_entries_per_fat_sector = 512 / (dir->fat->kind == FAT16 ? 2 : 4);

		u8 *cluster_buffer = malloc(cluster_size);
		if(!cluster_buffer)
			return (ResourceInfo) { .status = FAT_OUT_OF_MEMORY };

		while(next_cluster != last_cluster)
		{
			FatStatus read_status = read_cluster(dir->fat, next_cluster, cluster_buffer);
			if(read_status != FAT_OK)
			{
				free(cluster_buffer);
				return (ResourceInfo) { .status = read_status };
			}

			u32 next_cluster_location_sector = dir->fat->num_reserved_sectors
				+ next_cluster / cluster_entries_per_fat_sector;
			if(!dir->fat->fat_sector_is_valid
					|| dir->fat->sector_in_fat_buffer != next_cluster_location_sector)
			{
				MbrStatus read_status = MbrPartition_read_sector(
						dir->fat->partition,
						next_cluster_location_sector,
						dir->fat->fat_sector_buffer
					);
				if(read_status != MBR_OK)
				{
					free(cluster_buffer);
					return (ResourceInfo) { .status = FAT_MBR_ERROR };
				}
				dir->fat->fat_sector_is_valid = 1;
				dir->fat->sector_in_fat_buffer = next_cluster_location_sector;
			}
			u32 next_cluster_location_index = next_cluster % cluster_entries_per_fat_sector;
			if(dir->fat->kind == FAT16)
				next_cluster = ((u16*)dir->fat->fat_sector_buffer)[next_cluster_location_index];
			else	// FAT32
				next_cluster = ((u32*)dir->fat->fat_sector_buffer)[next_cluster_location_index]
					& 0xfffffff;

			//uart_dump_sector(UART0, cluster_buffer);

			for(unsigned entry_idx = 0; entry_idx < entries_per_cluster; entry_idx++)
			{
				ShortNameEntry *entry_info = (ShortNameEntry*)(cluster_buffer + entry_idx * 32);
				if(entry_info->name[0] == 0)
				{
					free(cluster_buffer);
					return (ResourceInfo) { .status = FAT_NO_OBJECT };
				}
				u8 matches = 1;
				for(unsigned pos = 0; pos < 11; pos++)
				{
					if(entry_info->name[pos] != name[pos])
					{
						matches = 0;
						break;
					}
				}

				if(matches)
				{
					ResourceInfo result = {
						.status = FAT_OK,
						.attributes = entry_info->attributes,
						.first_cluster = (u32)entry_info->first_cluster_high << 16
							| (u32)entry_info->first_cluster_low,
						.size = entry_info->file_size
					};
					free(cluster_buffer);
					return result;
				}
			}
		}
		free(cluster_buffer);
		return (ResourceInfo) { .status = FAT_NO_OBJECT };
	}
}

static ResourceInfo find_long_name_entry(FatDir *dir, Str name)
{
	UnicodeConversionResult conv_res = unicode_utf8_to_utf16(name);
	if(conv_res.status != UNICODE_OK)
		return (ResourceInfo) { .status = FAT_UNICODE_ERR };
	String name_utf16 = conv_res.result;
	if(dir->old_fat_root_dir)
	{
		u16 num_entries = dir->fat->old_fat_info.num_root_entries;
		u32 root_dir_size = (u32)num_entries * 32;
		u32 num_root_dir_sectors = root_dir_size / 512 + (root_dir_size % 512 ? 1 : 0);
		u32 root_buffer_size = num_root_dir_sectors * 512;
		u32 first_dir_sector = dir->fat->num_reserved_sectors +
			dir->fat->old_fat_info.fat_size * dir->fat->num_fats;

		u8 *root_dir_buffer = malloc(root_buffer_size);
		if(!root_dir_buffer)
		{
			String_drop(&name_utf16);
			return (ResourceInfo) { .status = FAT_OUT_OF_MEMORY };
		}

		for(u32 sector = 0; sector < num_root_dir_sectors; sector++)
		{
			MbrStatus read_st = MbrPartition_read_sector(
					dir->fat->partition,
					first_dir_sector + sector,
					root_dir_buffer + sector * 512
			);
			if(read_st != MBR_OK)
			{
				free(root_dir_buffer);
				String_drop(&name_utf16);
				return (ResourceInfo) { .status = FAT_MBR_ERROR };
			}
		}

		// size is more than maximum valid name to fit all bytes from the disk
		char *name_buffer = malloc(520);
		if(!name_buffer)
		{
			free(root_dir_buffer);
			String_drop(&name_utf16);
			return (ResourceInfo) { .status = FAT_OUT_OF_MEMORY };
		}

		u8 in_long_name_block = 0;

		for(u32 pos = 0; pos < num_entries; pos++)
		{
			ShortNameEntry *short_entry = (ShortNameEntry*)root_dir_buffer + pos;
			if(short_entry->name[0] == 0)
			{
				free(name_buffer);
				free(root_dir_buffer);
				String_drop(&name_utf16);
				return (ResourceInfo) { .status = FAT_NO_OBJECT };
			}
			else if(short_entry->name[0] == 0xe5)
				continue;
			else if((short_entry->attributes & FAT_ATTR_LONG_NAME_MASK) == FAT_ATTR_LONG_NAME)
			{
				LongNameEntry *long_entry = (LongNameEntry*)short_entry;
				if(long_entry->order & 0x40)
					in_long_name_block = 1;
				u8 order = long_entry->order & ~0x40;
				u32 start_in_name_buffer = ((u32)order - 1) * 26;
				memcpy(name_buffer + start_in_name_buffer, long_entry->name1, 10);
				memcpy(name_buffer + start_in_name_buffer + 10, long_entry->name2, 12);
				memcpy(name_buffer + start_in_name_buffer + 22, long_entry->name3, 4);
			}
			else if(in_long_name_block)
			{
				u32 name_length = unicode_utf16_strlen(name_buffer);
				if(name_length * 2 == name_utf16.length
						&& memory_equal((u8*)name_utf16.contents, (u8*)name_buffer, name_length * 2))
				{
					ResourceInfo result = {
						.status = FAT_OK,
						.size = short_entry->file_size,
						.first_cluster = short_entry->first_cluster_high << 16
							| short_entry->first_cluster_low,
						.attributes = short_entry->attributes
					};
					free(name_buffer);
					free(root_dir_buffer);
					String_drop(&name_utf16);
					return result;
				}
				in_long_name_block = 0;
			}
			else
				in_long_name_block = 0;
		}
		// no match found
		free(name_buffer);
		free(root_dir_buffer);
		String_drop(&name_utf16);
	}
	else	// FAT32 root or subdirectory
	{
		u32 cluster_size = 512 * dir->fat->sectors_per_cluster;
		u32 entries_per_cluster = cluster_size / 32;
		u32 next_cluster = dir->first_cluster & 0xfffffff;
		u32 last_cluster = last_fat_cluster(dir->fat);
		u32 cluster_entries_per_fat_sector = 512 / (dir->fat->kind == FAT16 ? 2 : 4);

		u8 *cluster_buffer = malloc(cluster_size);
		if(!cluster_buffer)
		{
			String_drop(&name_utf16);
			return (ResourceInfo) { .status = FAT_OUT_OF_MEMORY };
		}

		char *name_buffer = malloc(520);
		if(!name_buffer)
		{
			free(cluster_buffer);
			String_drop(&name_utf16);
			return (ResourceInfo) { .status = FAT_OUT_OF_MEMORY };
		}

		u8 in_long_name_block = 0;

		while(next_cluster != last_cluster)
		{
			FatStatus read_st = read_cluster(dir->fat, next_cluster, cluster_buffer);
			if(read_st != FAT_OK)
			{
				free(name_buffer);
				free(cluster_buffer);
				String_drop(&name_utf16);
				return (ResourceInfo) { .status = read_st };
			}

			u32 next_cluster_location_sector = dir->fat->num_reserved_sectors
				+ next_cluster / cluster_entries_per_fat_sector;
			if(!dir->fat->fat_sector_is_valid
					|| dir->fat->sector_in_fat_buffer != next_cluster_location_sector)
			{
				MbrStatus read_status = MbrPartition_read_sector(
						dir->fat->partition,
						next_cluster_location_sector,
						dir->fat->fat_sector_buffer
					);
				if(read_status != MBR_OK)
				{
					free(cluster_buffer);
					return (ResourceInfo) { .status = FAT_MBR_ERROR };
				}
				dir->fat->fat_sector_is_valid = 1;
				dir->fat->sector_in_fat_buffer = next_cluster_location_sector;
			}
			u32 next_cluster_location_index = next_cluster % cluster_entries_per_fat_sector;
			if(dir->fat->kind == FAT16)
				next_cluster = ((u16*)dir->fat->fat_sector_buffer)[next_cluster_location_index];
			else	// FAT32
				next_cluster = ((u32*)dir->fat->fat_sector_buffer)[next_cluster_location_index]
					& 0xfffffff;

			for(u32 pos = 0; pos < entries_per_cluster; pos++)
			{
				ShortNameEntry *short_entry = (ShortNameEntry*)cluster_buffer + pos;
				if(short_entry->name[0] == 0)
				{
					free(name_buffer);
					free(cluster_buffer);
					String_drop(&name_utf16);
					return (ResourceInfo) { .status = FAT_NO_OBJECT };
				}
				else if(short_entry->name[0] == 0xe5)
					continue;
				else if((short_entry->attributes & FAT_ATTR_LONG_NAME_MASK) == FAT_ATTR_LONG_NAME)
				{
					LongNameEntry *long_entry = (LongNameEntry*)short_entry;
					if(long_entry->order & 0x40)
						in_long_name_block = 1;
					u8 order = long_entry->order & ~0x40;
					u32 start_in_name_buffer = ((u32)order - 1) * 26;
					memcpy(name_buffer + start_in_name_buffer, long_entry->name1, 10);
					memcpy(name_buffer + start_in_name_buffer + 10, long_entry->name2, 12);
					memcpy(name_buffer + start_in_name_buffer + 22, long_entry->name3, 4);
				}
				else if(in_long_name_block)
				{
					u32 name_length = unicode_utf16_strlen(name_buffer);
					if(name_length * 2 == name_utf16.length
							&& memory_equal((u8*)name_utf16.contents, (u8*)name_buffer, name_length * 2))
					{
						ResourceInfo result = {
							.status = FAT_OK,
							.size = short_entry->file_size,
							.first_cluster = short_entry->first_cluster_high << 16
								| short_entry->first_cluster_low,
							.attributes = short_entry->attributes
						};
						free(name_buffer);
						free(cluster_buffer);
						String_drop(&name_utf16);
						return result;
					}
					in_long_name_block = 0;
				}
				else
					in_long_name_block = 0;
			}
		}
		free(name_buffer);
		free(cluster_buffer);
		String_drop(&name_utf16);
	}
	return (ResourceInfo) { .status = FAT_NO_OBJECT };
}

static FatStatus read_cluster(Fat *fat, u32 cluster_num, u8 *buffer)
{
	u32 first_cluster_offset = fat->num_reserved_sectors
		+ (fat->kind == FAT32 ? fat->fat32_info.fat_size : (u32)fat->old_fat_info.fat_size)
			* fat->num_fats
		+ (cluster_num - 2) * fat->sectors_per_cluster;
	for(unsigned sector = 0; sector < fat->sectors_per_cluster; sector++)
	{
		MbrStatus read_status = MbrPartition_read_sector(
				fat->partition,
				first_cluster_offset + sector,
				buffer + sector * 512
			);
		if(read_status != MBR_OK)
			return FAT_MBR_ERROR;
	}
	return FAT_OK;
}

static u32 last_fat_cluster(Fat *fat)
{
	switch(fat->kind)
	{
	case FAT12:
		return 0xfff;

	case FAT16:
		return  0xffff;

	case FAT32:
		return 0xfffffff;
	}
	return 0;
}
