.section .text.vectors
.global vectors
vectors:
	b asm_start
	b asm_undefined_instruction	// undef. instruction
	b .		// hyp call
	b asm_prefetch_abort	// supervisor call
	b asm_data_abort	// data abort
	b .		// Hyp trap
	b asm_on_irq	// IRQ
	b .     // FIQ
