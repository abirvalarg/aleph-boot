#include "string.h"
#include "heap.h"
#include "mem.h"

Str Str_from_cstr(char *contents)
{
	u32 length = 0;
	for(; contents[length]; length++);
	return (Str) {
		.pointer = contents,
		.length = length
	};
}

int Str_contains(Str str, char ch)
{
	for(u32 pos = 0; pos < str.length; pos++)
		if(str.pointer[pos] == ch)
			return 1;
	return 0;
}

StringResult String_new(u32 capacity)
{
	if(capacity)
	{
		char *contents = malloc(capacity);
		if(!contents)
			return (StringResult) { .status = STRING_OUT_OF_MEMORY };
		return (StringResult) {
			.status = STRING_OK,
			.result = {
				.contents = contents,
				.length = 0,
				.capacity = capacity
			}
		};
	}
	else
	{
		return (StringResult) {
			.status = STRING_OK,
			.result = { 0 }
		};
	}
}

void String_drop(String *string)
{
	if(string->contents)
		free(string->contents);
}

StringStatus String_push(String *string, char ch)
{
	if(string->length == string->capacity)
	{
		u32 new_capacity = (string->capacity << 1) + 1;
		if(new_capacity <= string->capacity)
			return STRING_OUT_OF_MEMORY;
		char *new_contents = malloc(new_capacity);
		if(!new_contents)
			return STRING_OUT_OF_MEMORY;
		memcpy(new_contents, string->contents, string->length);
		free(string->contents);
		string->contents = new_contents;
		string->capacity = new_capacity;
	}
	string->contents[string->length++] = ch;
	return STRING_OK;
}

StringStatus String_append(String *string, Str other)
{
	u32 new_length = string->length + other.length;
	if(new_length > string->capacity)
	{
		char *new_contents = malloc(new_length);
		if(!new_contents)
			return STRING_OUT_OF_MEMORY;
		memcpy(new_contents, string->contents, string->length);
		free(string->contents);
		string->contents = new_contents;
		string->capacity = new_length;
	}
	memcpy(string->contents + string->length, other.pointer, other.length);
	string->length = new_length;
	return STRING_OK;
}
