#pragma once
#include "types.h"

typedef volatile u32 IntcRegs;

void intc_reset(void);
void intc_unmask(u8 irq);
void intc_mask(u8 irq);
u8 intc_current_irq(void);
void intc_next_interrupt(void);

#define IRQ_MMCSD1 28
#define IRQ_MMCSD0 64

#define INTC_SYSCONFIG (0x10 / 4)
#define INTC_SYSSTATUS (0x14 / 4)
#define INTC_SIR_IRQ (0x40 / 4)
#define INTC_CONTROL (0x48 / 4)
#define INTC_MIR_CLEAR0 (0x88 / 4)
#define INTC_MIR_SET0 (0x8c / 4)
#define INTC_MIR_CLEAR1 (0xa8 / 4)
#define INTC_MIR_SET1 (0xac / 4)
#define INTC_MIR_CLEAR2 (0xc8 / 4)
#define INTC_MIR_SET2 (0xcc / 4)
#define INTC_MIR_CLEAR3 (0xe8 / 4)
#define INTC_MIR_SET3 (0xec / 4)

extern const u32 INTC_MIR_CLEAR_REGS[4];
extern const u32 INTC_MIR_SET_REGS[4];

#define INTC ((IntcRegs*)0x48200000)
