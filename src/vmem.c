#include "vmem.h"
#include "types.h"

#define KERNEL_MEMORY_START ((void*)0x80000000)
#define NUM_TEMORARY_PAGE_TABLES 8

#define K * 1024
#define M * 1048576

_Static_assert(NUM_TEMORARY_PAGE_TABLES <= 8, "Too many temporary page tables");

typedef enum AlignmentKind
{
	ALIGN_SMALL_PAGE,
	ALIGN_LARGE_PAGE,
	ALIGN_SECTION,
	ALIGN_SUPERSECTION
} AlignmentKind;

// number of KiBs in reserved area
static u32 reserved_size;
static u32 kib_search_starting_point;
static u32 total_mem_size;
__attribute__((aligned(8192)))
static u32 temporary_table1[2048];
__attribute__((aligned(1024)))
static u32 temporary_level2_tables[NUM_TEMORARY_PAGE_TABLES][256];
static u8 temporary_level2_tables_used;

/* Find some free space in RAM with given alignment.
 *
 * @param u32 size_and_align: size and alignment for the chunk
 *
 * @returns the physical address of the found space or 0 if space isn't found
 */
static u32 find_free_space(u32 size_and_align);

void vmem_table1_level1_clear(void)
{
	u32 *table = KERNEL_MEMORY_START;
	for(u32 pos = 0; pos < 4096; pos++)
		table[pos] = 0;
}

void vmem_fill_used_bitfield(u32 mem_size)
{
	u8 *start_of_bitfield = KERNEL_MEMORY_START + 16 * 1024;
	u32 num_kibs = mem_size / 8192;
	reserved_size = 16 + num_kibs;
	for(u32 pos = 0; pos < reserved_size / 8; pos++)
		start_of_bitfield[pos] = 0xff;
	for(u32 pos = reserved_size / 8; pos < num_kibs * 1024; pos++)
		start_of_bitfield[pos] = 0;
	kib_search_starting_point = 0;
	total_mem_size = mem_size;
}

void vmem_map_ram(u32 phys_ram_start, u32 mem_size)
{
	u32 num_supersections = mem_size / 16 + (mem_size % 16 ? 1 : 0);
	for(u32 supersection = 0; supersection < num_supersections; supersection++)
	{
		u32 offset = supersection * 16 * 1024 * 1024;
		vmem_map_kernel_supersection(phys_ram_start + offset, (u32)KERNEL_MEMORY_START + offset);
	}
}

void vmem_map_kernel_supersection(u32 phys_addr, u32 virt_addr)
{
	u32 table_offset = virt_addr / 1024 / 1024;
	u32 *section_table = KERNEL_MEMORY_START;
	for(unsigned section = 0; section < 16; section++)
	{
		// TEX = 000
		// C = 1
		// B = 1
		// S = 1
		// AP = 001
		section_table[table_offset + section] =
			0	// PXN
			| 1 << 1	// supersection
			| 1 << 2	// B
			| 1 << 3	// C
			| 1 << 4	// XN
			| 0 << 5	// PA[39:36]
			| 1 << 10	// AP[1:0]
			| 0 << 12	// TEX
			| 0 << 15	// AP[2]
			| 1 << 16	// S
			| 0 << 17	// nG
			| 1 << 18	// supersection
			| 0 << 19	// NS
			| 0 << 20	// PA[35:32]
			| phys_addr;
	}
}

VMemStatus vmem_map_kernel_space(u32 virt_start, u32 size, VMemAttr attr)
{
	AlignmentKind align_kind = ALIGN_SMALL_PAGE;
	if(size >= 16 M && virt_start % (16 M) == 0)
		align_kind = ALIGN_SUPERSECTION;
	else if(size >= 1 M && virt_start % (1 M) == 0)
		align_kind = ALIGN_SECTION;
	else if(size >= 64 K && virt_start % (64 K) == 0)
		align_kind = ALIGN_LARGE_PAGE;

	u32 num_supersections = align_kind == ALIGN_SUPERSECTION ? size / (16 M) : 0;

	u32 num_sections = align_kind == ALIGN_SECTION ? size / (1 M) :
		(align_kind == ALIGN_SUPERSECTION ? size % (16 M) / (1 M) : 0);

	u32 num_large_pages = align_kind == ALIGN_LARGE_PAGE ? size / (64 K) :
		(align_kind == ALIGN_SECTION || align_kind == ALIGN_SUPERSECTION ?
			size % (1 M) / (64 K) : 0);

	u32 num_small_pages = (align_kind == ALIGN_SMALL_PAGE ? size / (4 K) : size % (64 K) / (4 K))
		+ (size % (4 K) ? 1 : 0);

	u32 virt_pos = virt_start;
	u32 *table1 = KERNEL_MEMORY_START;

	// TEX = 000
	// C = 1
	// B = 1
	// S = 1
	// AP = attr.writable ? 001 : 101

	for(u32 supersection = 0; supersection < num_supersections; supersection++)
	{
		u32 phys_addr = find_free_space(16 K);
		if(phys_addr == 0)
			return VMEM_OUT_OF_MEMORY;

		u32 table_index = virt_pos / (1 M);
		for(u32 i = 0; i < 16; i++)
			table1[table_index + 1] =
				0	// PXN
				| 1 << 1	// supersection
				| 1 << 2	// B
				| 1 << 3	// C
				| (attr.executable ? 0 : 1) << 4	// XN
				| 1 << 10	// AP [1:0] = 01
				| 0 << 12	// TEX
				| (attr.writable ? 0 : 1) << 15	// AP[2]
				| 1 << 16	// S
				| 0 << 17	// nG
				| 1 << 18	// supersection
				| phys_addr;
		virt_pos += 16 M;
	}

	for(u32 section = 0; section < num_sections; section++)
	{
		u32 phys_addr = find_free_space(1 K);
		if(phys_addr == 0)
			return VMEM_OUT_OF_MEMORY;

		u32 table_index = virt_pos / (1 M);
		table1[table_index] =
			0	// PXN
			| 1 << 1	// section
			| 1 << 2	// B
			| 1 << 3	// C
			| (attr.executable ? 0 : 1) << 4	// XN
			| 1 << 10	// AP[1:0] = 01
			| 0 << 12	// TEX
			| (attr.writable ? 0 : 1) << 15	// AP[2]
			| 1 << 16	// S
			| 0 << 17	// nG
			| phys_addr;
		virt_pos += 1 M;
	}

	for(u32 page = 0; page < num_large_pages; page++)
	{
		u32 phys_addr = find_free_space(64);
		if(phys_addr == 0)
			return VMEM_OUT_OF_MEMORY;

		u32 table1_index = virt_pos / (1 M);
		u32 *table2 = 0;
		if((table1[table1_index] & 0b11) == 0b01)
			table2 = (u32*)(table1[table1_index] & 0xfffffc00);
		else
		{
			table2 = (u32*)find_free_space(1);
			if(!table2)
				return VMEM_OUT_OF_MEMORY;
			table1[table1_index] =
				0b01	// page table
				| 0 << 2	// PXN
				| (u32)table2;
		}
		u32 table2_index = virt_pos % (1 M) / (4 K);
		for(u32 i = 0; i < 16; i++)
			table2[table2_index + i] =
				0b01	// large page
				| 1 << 2	// B
				| 1 << 1	// C
				| 1 << 4	// AP[1:0] = 01
				| (attr.writable ? 0 : 1) << 9	// AP[2]
				| 1 << 10	// S
				| 0 << 11	// nG
				| 0 << 12	// TEX
				| (attr.executable ? 0 : 1) << 15	// XN
				| phys_addr;
		virt_pos += 64 K;
	}

	for(u32 page = 0; page < num_small_pages; page++)
	{
		u32 phys_addr = find_free_space(4);
		if(phys_addr == 0)
			return VMEM_OUT_OF_MEMORY;

		u32 table1_index = virt_pos / (1 M);
		u32 *table2 = 0;
		if((table1[table1_index] & 0b11) == 0b01)
			table2 = (u32*)(table1[table1_index] & 0xfffffc00);
		else
		{
			table2 = (u32*)find_free_space(1);
			if(!table2)
				return VMEM_OUT_OF_MEMORY;
			table1[table1_index] =
				0b01	// page table
				| 0 << 2	// PXN
				| (u32)table2;
		}
		u32 table2_index = virt_pos % (1 M) / (4 K);
		table2[table2_index] =
			(attr.executable ? 0 : 1)	// XN
			| 1 << 1	// small page
			| 1 << 2	// B
			| 1 << 3	// C
			| 1 << 4	// AP[1:0]
			| 0 << 6	// TEX
			| (attr.writable ? 0 : 1) << 9	// AP[2]
			| 1 << 10	// S
			| 0 << 11	// nG
			| phys_addr;
		virt_pos += 4 K;
	}

	asm("dsb");
	// invalidate entire TLB
	asm("mcr p15, 0, %0, c8, c7, 0"
			:
			: "r" (1)
		);
	asm("dsb");
	asm("isb");

	return VMEM_OK;
}

void vmem_change_kernel_space_attr(u32 virt_start, u32 size, VMemAttr attr)
{
	AlignmentKind align_kind = ALIGN_SMALL_PAGE;
	if(size >= 16 M && virt_start % (16 M) == 0)
		align_kind = ALIGN_SUPERSECTION;
	else if(size >= 1 M && virt_start % (1 M) == 0)
		align_kind = ALIGN_SECTION;
	else if(size >= 64 K && virt_start % (64 K) == 0)
		align_kind = ALIGN_LARGE_PAGE;

	u32 num_supersections = align_kind == ALIGN_SUPERSECTION ? size / (16 M) : 0;

	u32 num_sections = align_kind == ALIGN_SECTION ? size / (1 M) :
		(align_kind == ALIGN_SUPERSECTION ? size % (16 M) / (1 M) : 0);

	u32 num_large_pages = align_kind == ALIGN_LARGE_PAGE ? size / (64 K) :
		(align_kind == ALIGN_SECTION || align_kind == ALIGN_SUPERSECTION ?
			size % (1 M) / (64 K) : 0);

	u32 num_small_pages = (align_kind == ALIGN_SMALL_PAGE ? size / (4 K) : size % (64 K) / (4 K))
		+ (size % (4 K) ? 1 : 0);

	u32 virt_pos = virt_start;
	u32 *table1 = KERNEL_MEMORY_START;

	for(u32 supersection = 0; supersection < num_supersections; supersection++)
	{
		u32 index = virt_pos / (1 M);
		for(u32 subsection = 0; subsection < 16; subsection++)
		{
			table1[index + subsection] &= ~(
					1 << 4	// XN
					| 1 << 15	// AP[2] (not writable)
				);
			table1[index + subsection] |=
				(attr.executable ? 0 : 1) << 4	// XN
				| (attr.writable ? 0 : 1) << 15;	// AP[2]
		}
		virt_pos += 16 M;
	}

	for(u32 section = 0; section < num_sections; section++)
	{
		u32 index = virt_pos / (1 M);
		table1[index] &= ~(
				1 << 4	// XN
				| 1 << 15	// AP[2]
			);
		table1[index] |=
			(attr.executable ? 0 : 1) << 4	// XN
			| (attr.writable ? 0 : 1) << 15;	// AP[2]
		virt_pos += 1 M;
	}

	for(u32 page = 0; page < num_large_pages; page++)
	{
		u32 index1 = virt_pos / (1 M);
		u32 index2 = virt_pos % (1 M) / (4 K);
		u32 *table2 = (u32*)(table1[index1] & ~(0b1111111111));
		for(u32 subpage = 0; subpage < 16; subpage++)
		{
			table2[index2 + subpage] &= ~(
					1 << 9	// AP[2]
					| 1 << 15	// XN
				);
			table2[index2 + subpage] |=
				(attr.writable ? 0 : 1) << 9	// AP[2]
				| (attr.executable ? 0 : 1) << 15;	// XN
		}
		virt_pos += 64 K;
	}

	for(u32 page = 0; page < num_small_pages; page++)
	{
		u32 index1 = virt_pos / (1 M);
		u32 index2 = virt_pos % (1 M) / (4 K);
		u32 *table2 = (u32*)(table1[index1] & ~(0b1111111111));
		table2[index2] &= ~(
				1	// XN
				| 1 << 9	// AP[2]
			);
		table2[index2] |=
			(attr.executable ? 0 : 1)	// XN
			| (attr.writable ? 0 : 1) << 9;	// AP[2]
		virt_pos += 4 K;
	}

	asm("dsb");
	// invalidate entire TLB
	asm("mcr p15, 0, %0, c8, c7, 0"
			:
			: "r" (1)
		);
	asm("dsb");
	asm("isb");
}

void vmem_clear_temporary_tables(void)
{
	temporary_level2_tables_used = 0;
	for(unsigned i = 0; i < 2048; i++)
		temporary_table1[i] = 0;
}

VMemStatus vmem_map_temporary_large_page(u32 phys_addr, u32 virt_addr)
{
	u32 level1_index = virt_addr / 1024 / 1024;
	u32 *temp_page_table = 0;
	if((temporary_table1[level1_index] & 0b11) == 0b01)	// is a page table
		temp_page_table = (u32*)(temporary_table1[level1_index] & 0xfffffc00);
	else
	{
		for(u32 temp_table_idx = 0; temp_table_idx < NUM_TEMORARY_PAGE_TABLES; temp_table_idx++)
		{
			if(!(temporary_level2_tables_used & (1 << temp_table_idx)))	// temp table is not used
			{
				temporary_level2_tables_used |= 1 << temp_table_idx;
				temp_page_table = temporary_level2_tables[temp_table_idx];
				break;
			}
		}
		if(!temp_page_table)
			return VMEM_NO_TEMPORARY_TABLES;
		for(u32 i = 0; i < 256; i++)
			temp_page_table[i] = 0;
		temporary_table1[level1_index] =
			0b01	// page table
			| 0 << 2	// PXN
			| 0 << 3	// NS
			| 0 << 4	// SBZ ?
			| 0 << 5	// domain
			| (u32)temp_page_table;
	}
	// bits 19:12
	u32 level2_index = (virt_addr % (1024 * 1024)) >> 12;
	// TEX = 000
	// C = 1
	// B = 1
	// S = 1
	// AP = 001
	for(u32 subpage = 0; subpage < 16; subpage++)
		temp_page_table[level2_index + subpage] =
			0b01	// large page
			| 1 << 2	// B
			| 1 << 3	// C
			| 1 << 4	// AP[1:0]
			| 0b000 << 6	// SBZ ?
			| 0 << 9	// AP[2]
			| 1 << 10	// S
			| 0 << 11	// nG
			| 0 << 12	// TEX
			| 0 << 15	// XN
			| phys_addr;
	return VMEM_OK;
}

VMemStatus vmem_map_temporary_small_page(u32 phys_addr, u32 virt_addr)
{
	u32 level1_index = virt_addr / 1024 / 1024;
	u32 *temp_page_table = 0;
	if((temporary_table1[level1_index] & 0b11) == 0b01)	// is a page table
		temp_page_table = (u32*)(temporary_table1[level1_index] & 0xfffffc00);
	else
	{
		for(u32 temp_table_idx = 0; temp_table_idx < NUM_TEMORARY_PAGE_TABLES; temp_table_idx++)
		{
			if(!(temporary_level2_tables_used & (1 << temp_table_idx)))	// temp table is not used
			{
				temporary_level2_tables_used |= 1 << temp_table_idx;
				temp_page_table = temporary_level2_tables[temp_table_idx];
				break;
			}
		}
		if(!temp_page_table)
			return VMEM_NO_TEMPORARY_TABLES;
		for(u32 i = 0; i < 256; i++)
			temp_page_table[i] = 0;
		temporary_table1[level1_index] =
			0b01	// page table
			| 0 << 2	// PXN
			| 0 << 3	// NS
			| 0 << 4	// SBZ ?
			| 0 << 5	// domain
			| (u32)temp_page_table;
	}
	// bits 19:12
	u32 level2_index = (virt_addr % (1024 * 1024)) >> 12;
	// TEX = 000
	// C = 1
	// B = 1
	// S = 1
	// AP = 001
	temp_page_table[level2_index] =
		0	// XN
		| 1 << 1	// small page
		| 1 << 2	// B
		| 1 << 3	// C
		| 1 << 4	// AP[1:0]
		| 0 << 6	// TEX
		| 0 << 9	// AP[2]
		| 1 << 10	// S
		| 0 << 11	// nG
		| phys_addr;
	return VMEM_OK;
}

VMemStatus vmem_map_temporary_small_page_device(u32 phys_addr, u32 virt_addr)
{
	u32 level1_index = virt_addr / 1024 / 1024;
	u32 *temp_page_table = 0;
	if((temporary_table1[level1_index] & 0b11) == 0b01)	// is a page table
		temp_page_table = (u32*)(temporary_table1[level1_index] & 0xfffffc00);
	else
	{
		for(u32 temp_table_idx = 0; temp_table_idx < NUM_TEMORARY_PAGE_TABLES; temp_table_idx++)
		{
			if(!(temporary_level2_tables_used & (1 << temp_table_idx)))	// temp table is not used
			{
				temporary_level2_tables_used |= 1 << temp_table_idx;
				temp_page_table = temporary_level2_tables[temp_table_idx];
				break;
			}
		}
		if(!temp_page_table)
			return VMEM_NO_TEMPORARY_TABLES;
		for(u32 i = 0; i < 256; i++)
			temp_page_table[i] = 0;
		temporary_table1[level1_index] =
			0b01	// page table
			| 0 << 2	// PXN
			| 0 << 3	// NS
			| 0 << 4	// SBZ ?
			| 0 << 5	// domain
			| (u32)temp_page_table;
	}
	// bits 19:12
	u32 level2_index = (virt_addr % (1024 * 1024)) >> 12;
	// TEX = 000
	// C = 0
	// B = 1
	// S = 0
	// AP = 001
	temp_page_table[level2_index] =
		1	// XN
		| 1 << 1	// small page
		| 1 << 2	// B
		| 0 << 3	// C
		| 1 << 4	// AP[1:0]
		| 0 << 6	// TEX
		| 0 << 9	// AP[2]
		| 0 << 10	// S
		| 0 << 11	// nG
		| phys_addr;
	return VMEM_OK;
}

void vmem_start(void)
{
	// enable client mode for domain 0
	asm("mcr p15, 0, %0, c3, c0, 0"
			:
			: "r" (0b01010101010101010101010101010101)
	);
	asm("mcr p15, 0, %0, c2, c0, 2"
			:
			: "r" (1)
	);
	u32 ttbr0 =
		1	// C
		| 1 << 1	// S
		| 1 << 3	// RGN (Normal memory, Outer Write-Back Write-Allocate Cacheable.)
		| 0 << 5	// NOS
		| (u32)temporary_table1;
	asm("mcr p15, 0, %0, c2, c0, 0"
			:
			: "r" (ttbr0)
	);
	u32 ttbr1 =
		1	// C
		| 1 << 1	// S
		| 1 << 3	// RGN
		| 0 << 5	// NOS
		| (u32)KERNEL_MEMORY_START;
	asm("mcr p15, 0, %0, c2, c0, 1"
			:
			: "r" (ttbr1)
	);
	u32 syscr;
	asm("mrc p15, 0, %0, c1, c0, 0"
			: "=r" (syscr)
	);
	syscr &= ~(1 << 28 | 1 << 12 | 1 << 11 | 1 << 2);
	syscr |=
		1 << 12	// instruction cache enable
		| 1 << 11	// branch predictor enable
		| 1 << 2	// cache enable
		| 1;	// MMU enable
	asm("mcr p15, 0, %0, c1, c0, 0"
			:
			: "r" (syscr)
	);
}

void vmem_stop(void)
{
	u32 syscr;
	asm("mrc p15, 0, %0, c1, c0, 0"
			: "=r" (syscr)
		);
	syscr &= ~(
			1 << 12
			| 1 << 11
			| 1 << 2
			| 1
		);
	asm("mcr p15, 0, %0, c1, c0, 0"
			:
			: "r" (syscr)
		);
}

// FIXME wrong addresses when requesting 1MiB
// TODO rewrite this function completely?
static u32 find_free_space(u32 size_and_align)
{
	// address of a candidate
	u32 addr = kib_search_starting_point * 1024;
	u8 *bitfield = KERNEL_MEMORY_START + 16 K;
	u8 skipped_free = 0;
	while(addr < total_mem_size K)
	{
		u32 next_addr;
		u8 aligned;
		u8 used = 0;
		if(addr % (size_and_align K) == 0)
		{
			aligned = 1;
			next_addr = addr + size_and_align K;
		}
		else
		{
			aligned = 0;
			next_addr = addr + size_and_align K - addr % (size_and_align K);
		}
		for(u32 check_addr = addr; check_addr < next_addr; check_addr += 1 K)
		{
			u32 bitfield_index = check_addr / 1024;
			u32 byte = bitfield_index / 8;
			if(bitfield[byte] & 1 << (bitfield_index % 8))
				used = 1;
			else
				skipped_free = 1;
			if(used && skipped_free)
				break;
		}
		if(aligned && !used)
		{
			if(!skipped_free)
				kib_search_starting_point = next_addr / 1024;
			for(u32 check_addr = addr; check_addr < next_addr; check_addr += 1 K)
			{
				u32 bitfield_index = check_addr / 1024;
				u32 byte = bitfield_index / 8;
				bitfield[byte] |= 1 << (bitfield_index % 8);
			}
			/*
			uart_send_str(UART0, "[VMEM] found free address 0x");
			uart_send_usize(UART0, addr);
			uart_send_str(UART0, "\r\n");
			*/
			return addr + (u32)KERNEL_MEMORY_START;
		}
		addr = next_addr;
	}
	return 0;
}
