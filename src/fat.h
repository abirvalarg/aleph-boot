#pragma once
#include "mbr.h"
#include "string.h"

#define FAT_ATTR_READ_ONLY 1
#define FAT_ATTR_HIDDEN 2
#define FAT_ATTR_SYSTEM 4
#define FAT_ATTR_VOLUME_ID 8
#define FAT_ATTR_DIRECTORY 0x10
#define FAT_ATTR_ARCHIVE 0x20

#define FAT_ATTR_LONG_NAME (FAT_ATTR_READ_ONLY | FAT_ATTR_HIDDEN | FAT_ATTR_SYSTEM \
		| FAT_ATTR_VOLUME_ID)
#define FAT_ATTR_LONG_NAME_MASK (FAT_ATTR_READ_ONLY | FAT_ATTR_HIDDEN | FAT_ATTR_SYSTEM \
		| FAT_ATTR_VOLUME_ID | FAT_ATTR_DIRECTORY | FAT_ATTR_ARCHIVE)

typedef enum FatKind
{
	FAT12,
	FAT16,
	FAT32
} FatKind;

typedef struct Fat
{
	MbrPartition partition;
	u8 *fat_sector_buffer;
	u32 sector_in_fat_buffer;
	u32 num_clusters;
	union
	{
		struct
		{
			u16 num_root_entries;
			u16 fat_size;
		} old_fat_info;
		struct
		{
			u32 fat_size;
			u32 root_dir_first_cluster;
		} fat32_info;
	};
	FatKind kind;
	u16 num_reserved_sectors;
	u8 sectors_per_cluster;
	u8 num_fats;
	u8 fat_sector_is_valid;
} Fat;

typedef struct FatDir
{
	Fat *fat;
	u32 first_cluster;
	u8 old_fat_root_dir;
} FatDir;

typedef struct FatFile
{
	Fat *fat;
	u8 *read_buffer;
	u32 buffer_size;
	u32 buffer_start;
	u32 next_cluster;
	u32 left_on_disk;
} FatFile;

typedef enum FatStatus
{
	FAT_OK,
	FAT_OUT_OF_MEMORY,
	FAT_MBR_ERROR,
	FAT_WRONG_OBJECT_KIND,
	FAT_NO_OBJECT,
	FAT_UNICODE_ERR,
	FAT_NOT_IMPLEMENTED
} FatStatus;

typedef struct FatDirResult
{
	FatDir result;
	FatStatus status;
} FatDirResult;

typedef struct FatFileResult
{
	FatFile *result;
	FatStatus status;
} FatFileResult;

typedef struct FatFileReadResult
{
	u32 length;
	FatStatus status;
} FatFileReadResult;

/* Initialize the `Fat` structure with the filesystem data.
 *
 * may return the following status codes:
 * 	FAT_OK
 * 	FAT_OUT_OF_MEMORY
 * 	FAT_MBR_ERROR
 * 	FAT_NOT_IMPLEMENTED (doesn't support FAT12)
 */
FatStatus Fat_init(Fat *fat, MbrPartition partition);

// Deinitialize the `Fat` structure, freeing all resources
void Fat_deinit(Fat *fat);

// Open root directory. This function can't fail because root directory always exists
FatDir Fat_open_root_dir(Fat *fat);

/* Open a file located in the chosen directory.
 *
 * may return the following status codes:
 * 	FAT_OK
 * 	FAT_OUT_OF_MEMORY
 * 	FAT_MBR_ERROR
 * 	FAT_WRONG_OBJECT_KIND (found subdirectory)
 * 	FAT_NO_OBJECT
 */
FatFileResult FatDir_open(FatDir *dir, Str name);

/* Open a subdirectory in a given directory.
 *
 * may return the following status codes:
 * 	FAT_OK
 * 	FAT_OUT_OF_MEMORY
 * 	FAT_MBR_ERROR
 * 	FAT_WRONG_OBJECT_KIND (found a file)
 * 	FAT_NO_OBJECT
 */
FatDirResult FatDir_open_dir(FatDir *dir, Str name);

// Close a file, releasing all associated resources
void FatFile_close(FatFile *fp);

/* Read a piece of file
 *
 * may return the following status codes:
 * 	FAT_OK
 *	FAT_MBR_ERROR
 * 
 * if `result.length` == 0, file is over
 */
FatFileReadResult FatFile_read(FatFile *fp, u8 *buffer, u32 length);
