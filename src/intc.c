#include "intc.h"

void intc_reset(void)
{
	INTC[INTC_SYSCONFIG] = 1 << 1;
	while(INTC[INTC_SYSSTATUS] == 0);
}

void intc_unmask(u8 irq)
{
	u32 reg = INTC_MIR_CLEAR_REGS[irq / 32];
	u32 pos = irq % 32;
	INTC[reg] = 1 << pos;
}

void intc_mask(u8 irq)
{
	u32 reg = INTC_MIR_SET_REGS[irq / 32];
	u32 pos = irq % 32;
	INTC[reg] = 1 << pos;
}

u8 intc_current_irq(void)
{
	return INTC[INTC_SIR_IRQ] & 0x7f;
}

void intc_next_interrupt(void)
{
	INTC[INTC_CONTROL] = 1;
}

const u32 INTC_MIR_CLEAR_REGS[4] = {
	INTC_MIR_CLEAR0,
	INTC_MIR_CLEAR1,
	INTC_MIR_CLEAR2,
	INTC_MIR_CLEAR3
};

const u32 INTC_MIR_SET_REGS[4] = {
	INTC_MIR_SET0,
	INTC_MIR_SET1,
	INTC_MIR_SET2,
	INTC_MIR_SET3
};
