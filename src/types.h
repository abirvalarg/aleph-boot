#pragma once

typedef unsigned char u8;
typedef signed char i8;
typedef i8 s8;
typedef unsigned short u16;
typedef short i16;
typedef unsigned u32;
typedef u32 usize;
typedef int i32;
typedef unsigned long long u64;

_Static_assert(sizeof(u8) == 1, "Bad size of u8");
_Static_assert(sizeof(i8) == 1, "Bad size of s8");
_Static_assert(sizeof(u16) == 2, "Bad size of u16");
_Static_assert(sizeof(i16) == 2, "Bad size of i16");
_Static_assert(sizeof(u32) == 4, "Bad size of u32");
_Static_assert(sizeof(i32) == 4, "Bad size of i32");
_Static_assert(sizeof(u64) == 8, "Bad size of u64");
