#pragma once
#include "../config.h"
#include "types.h"

typedef volatile u32 GpioRegs;

typedef enum GpioPinMode
{
    PIN_MODE_OUTPUT = 0,
    PIN_MODE_INPUT = 1
} GpioPinMode;

void gpio_pin_mode(GpioRegs *gpio, u8 pin, GpioPinMode mode);
void gpio_pin_write(GpioRegs *gpio, u8 pin, int state);

#define GPIO_OE (0x134 / 4)
#define GPIO_CLEARDATAOUT (0x190 / 4)
#define GPIO_SETDATAOUT (0x194 / 4)

#define GPIO0 ((GpioRegs*)0x44E07000)
#define GPIO1 ((GpioRegs*)0x4804C000)
