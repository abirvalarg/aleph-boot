#pragma once
#include "types.h"

typedef enum VMemStatus
{
	VMEM_OK,
	VMEM_NO_TEMPORARY_TABLES,
	VMEM_OUT_OF_MEMORY
} VMemStatus;

typedef struct VMemAttr
{
	u8 executable: 1;
	u8 writable: 1;
} VMemAttr;

void vmem_table1_level1_clear(void);
/// gets size of total memory in KiBs
void vmem_fill_used_bitfield(u32 mem_size);
/* Map all RAM to the beginning of the kernel space
 *
 * @param u32 phys_ram_start: first address of the physical RAM
 * @param u32 mem_size: size of the RAM in MiBs
 */
void vmem_map_ram(u32 phys_ram_start, u32 mem_size);
void vmem_map_kernel_supersection(u32 phys_addr, u32 virt_addr);
/*
 * May return following codes:
 * 	VMEM_OK
 * 	VMEM_OUT_OF_MEMORY
 */
VMemStatus vmem_map_kernel_space(u32 virt_start, u32 size, VMemAttr attr);
void vmem_change_kernel_space_attr(u32 virt_start, u32 size, VMemAttr attr);
void vmem_clear_temporary_tables(void);
/*
 * May return following codes:
 * 	VMEM_OK
 * 	VMEM_NO_TEMPORARY_TABLES
 */
VMemStatus vmem_map_temporary_large_page(u32 phys_addr, u32 virt_addr);
/*
 * May return following codes:
 * 	VMEM_OK
 * 	VMEM_NO_TEMPORARY_TABLES
 */
VMemStatus vmem_map_temporary_small_page(u32 phys_addr, u32 virt_addr);
/*
 * May return following codes:
 * 	VMEM_OK
 * 	VMEM_NO_TEMPORARY_TABLES
 */
VMemStatus vmem_map_temporary_small_page_device(u32 phys_addr, u32 virt_addr);

void vmem_start(void);
void vmem_stop(void);
