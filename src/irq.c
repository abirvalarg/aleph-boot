#include "irq.h"

static unsigned num_disables = 0;

void irq_disable(void)
{
	asm("cpsid i");
	num_disables++;
}

void irq_enable(void)
{
	if(--num_disables == 0)
		asm("cpsie i");
}
