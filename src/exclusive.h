#pragma once

#define EXCLUSIVE_LOAD_WORD(var, addr) asm volatile("ldrex %0, %1" : "=r" (var) : "m" (addr))
#define EXCLUSIVE_STORE_WORD(not_success, var, dest) asm volatile("strex %0, %1, %2" : "=&r" (not_success) : "r" (var), "m" (dest))
