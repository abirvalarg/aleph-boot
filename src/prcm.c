#include "prcm.h"
#include "exclusive.h"

void prcm_config_core_pll(const PllConfig *cfg, u8 m4, u8 m5, u8 m6)
{
    u32 not_success;
    // disable PLL
    do
    {
        u32 reg;
        EXCLUSIVE_LOAD_WORD(reg, prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_CORE]);
        reg &= ~PRCM_DPLL_EN_MASK;
        reg |= PLL_MODE_BYPASS << PRCM_DPLL_EN_SHIFT;
        EXCLUSIVE_STORE_WORD(not_success, reg, prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_CORE]);
    }
    while(not_success);

    // wait for PLL to switch to bypass
    while(prcm.cm_wkup[PRCM_CM_IDLEST_DPLL_CORE] >> 8 == 0);

    // setup multiply and divide
    do
    {
        u32 reg;
        EXCLUSIVE_LOAD_WORD(reg, prcm.cm_wkup[PRCM_CM_CLKSEL_DPLL_CORE]);
        reg &= ~(PRCM_DPLL_MULT_MASK | PRCM_DPLL_DIV_MASK);
        reg |= (cfg->m << PRCM_DPLL_MULT_SHIFT) | (cfg->n << PRCM_DPLL_DIV_SHIFT);
        EXCLUSIVE_STORE_WORD(not_success, reg, prcm.cm_wkup[PRCM_CM_CLKSEL_DPLL_CORE]);
    }
    while(not_success);

    // setup M4
    prcm.cm_wkup[PRCM_CM_DIV_M4_DPLL_CORE] = m4;

    // setup M5
    prcm.cm_wkup[PRCM_CM_DIV_M5_DPLL_CORE] = m5;

    // setup M6
    prcm.cm_wkup[PRCM_CM_DIV_M6_DPLL_CORE] = m6;

    // enable PLL
    do
    {
        u32 reg;
        EXCLUSIVE_LOAD_WORD(reg, prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_CORE]);
        reg &= ~PRCM_DPLL_EN_MASK;
        reg |= PLL_MODE_LOCK << PRCM_DPLL_EN_SHIFT;
        EXCLUSIVE_STORE_WORD(not_success, reg, prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_CORE]);
    }
    while(not_success);

    // wait for PLL
    while((prcm.cm_wkup[PRCM_CM_IDLEST_DPLL_CORE] & 1) == 0);
}

void prcm_config_peripheral_pll(const PllConfig *cfg)
{
    u32 not_success;

    // disable PLL
    do
    {
        u32 reg;
        EXCLUSIVE_LOAD_WORD(reg, prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_PER]);
        reg &= ~PRCM_DPLL_EN_MASK;
        reg |= PLL_MODE_BYPASS << PRCM_DPLL_EN_SHIFT;
        EXCLUSIVE_STORE_WORD(not_success, reg, prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_PER]);
    }
    while(not_success);

    // wait for PLL to switch to bypass
    while(prcm.cm_wkup[PRCM_CM_IDLEST_DPLL_PER] >> 8 == 0)
	{}

    // setup MULT and DIV and SD
	prcm.cm_wkup[PRCM_CM_CLKSEL_DPLL_PER] =
		cfg->m << PRCM_DPLL_MULT_SHIFT
		| cfg->n << PRCM_DPLL_DIV_SHIFT
		| cfg->sd << 24;

    if(cfg->m2 > 0)
        prcm.cm_wkup[PRCM_CM_DIV_M2_DPLL_PER] = cfg->m2;

    // enable PLL
    do
    {
        u32 reg;
        EXCLUSIVE_LOAD_WORD(reg, prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_PER]);
        reg &= ~PRCM_DPLL_EN_MASK;
        reg |= PLL_MODE_LOCK << PRCM_DPLL_EN_SHIFT;
        EXCLUSIVE_STORE_WORD(not_success, reg, prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_PER]);
    }
    while(not_success);

    // wait for PLL
    while((prcm.cm_wkup[PRCM_CM_IDLEST_DPLL_PER] & 1) == 0);
}

void prcm_config_mpu_pll(const PllConfig *cfg)
{
    u32 not_success;

    // disable PLL
    do
    {
        u32 reg;
        EXCLUSIVE_LOAD_WORD(reg, prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_MPU]);
        reg &= ~PRCM_DPLL_EN_MASK;
        reg |= PLL_MODE_BYPASS << PRCM_DPLL_EN_SHIFT;
        EXCLUSIVE_STORE_WORD(not_success, reg, prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_MPU]);
    }
    while(not_success);

    // wait for bypass mode
    while(prcm.cm_wkup[PRCM_CM_IDLEST_DPLL_MPU] >> 8 == 0);

    do
    {
        u32 reg;
        EXCLUSIVE_LOAD_WORD(reg, prcm.cm_wkup[PRCM_CM_CLKSEL_DPLL_MPU]);
        reg &= ~(PRCM_DPLL_MULT_MASK | PRCM_DPLL_DIV_MASK);
        reg |= (cfg->m << PRCM_DPLL_MULT_SHIFT) | (cfg->n << PRCM_DPLL_DIV_SHIFT);
        EXCLUSIVE_STORE_WORD(not_success, reg, prcm.cm_wkup[PRCM_CM_CLKSEL_DPLL_MPU]);
    }
    while(not_success);

    if(cfg->m2 > 0)
        prcm.cm_wkup[PRCM_CM_DIV_M2_DPLL_MPU] = cfg->m2;

    do
    {
        u32 reg;
        EXCLUSIVE_LOAD_WORD(reg, prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_MPU]);
        reg &= ~PRCM_DPLL_EN_MASK;
        reg |= PLL_MODE_LOCK << PRCM_DPLL_EN_SHIFT;
        EXCLUSIVE_STORE_WORD(not_success, reg, prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_MPU]);
    }
    while(not_success);

    // wait for PLL
    while((prcm.cm_wkup[PRCM_CM_IDLEST_DPLL_MPU] & 1) == 0);
}

void prcm_config_ddr_pll(const PllConfig *cfg)
{
    {
        u32 clkmode = prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_DDR];
        clkmode &= ~PRCM_DPLL_EN_MASK;
        clkmode |= PLL_MODE_BYPASS;
        prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_DDR] = clkmode;
    }

    while((prcm.cm_wkup[PRCM_CM_IDLEST_DPLL_DDR] & PLL_ST_MN_BYPASS_MASK) == 0)
    {}

    {
        u32 clksel = prcm.cm_wkup[PRCM_CM_CLKSEL_DPLL_DDR];
        clksel &= ~(PRCM_DPLL_MULT_MASK | PRCM_DPLL_DIV_MASK);
        clksel |= (cfg->m << PRCM_DPLL_MULT_SHIFT) | (cfg->n << PRCM_DPLL_DIV_SHIFT);
        prcm.cm_wkup[PRCM_CM_CLKSEL_DPLL_DDR] = clksel;
    }

    {
        u32 div_m2 = prcm.cm_wkup[PRCM_CM_DIV_M2_DPLL_DDR];
        div_m2 &= ~DIV_M2_CLKOUT_DIV_MASK;
        div_m2 |= cfg->m2 & DIV_M2_CLKOUT_DIV_MASK;
        prcm.cm_wkup[PRCM_CM_DIV_M2_DPLL_DDR] = div_m2;
    }

    prcm.cm_wkup[PRCM_CM_CLKMODE_DPLL_DDR] |= PLL_MODE_LOCK;

    while((prcm.cm_wkup[PRCM_CM_IDLEST_DPLL_DDR] & PLL_ST_DPLL_CLK) == 0);
}

void prcm_wkup_switch(PrcmWkupModule module, PrcmModuleMode mode)
{
    u32 not_success;
    do
    {
        u32 reg;
        EXCLUSIVE_LOAD_WORD(reg, prcm.cm_wkup[module]);
        reg &= ~0b11;
        reg |= mode;
        EXCLUSIVE_STORE_WORD(not_success, reg, prcm.cm_wkup[module]);
    } while(not_success);
}

void prcm_per_switch(PrcmPerModule module, PrcmModuleMode mode)
{
    u32 not_success;
    do
    {
        u32 reg;
        EXCLUSIVE_LOAD_WORD(reg, prcm.cm_per[module]);
        reg &= ~0b11;
        reg |= mode;
        EXCLUSIVE_STORE_WORD(not_success, reg, prcm.cm_per[module]);
    } while(not_success);
}

PrcmModuleIdleStatus prcm_wkup_module_idle_status(PrcmWkupModule module)
{
    return prcm.cm_wkup[module] >> 16;
}

PrcmRegs prcm = {
    .cm_per = (void*)0x44E00000,
    .cm_wkup = (void*)0x44E00400
};
