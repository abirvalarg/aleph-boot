#pragma once
#include "types.h"
#include "string.h"

typedef enum UnicodeStatus
{
	UNICODE_OK,
	UNICODE_CHAR_TOO_LONG,
	UNICODE_BAD_ENCODING,
	UNICODE_OUT_OF_MEMORY
} UnicodeStatus;

typedef struct UnicodeConversionResult
{
	String result;
	UnicodeStatus status;
} UnicodeConversionResult;

UnicodeConversionResult unicode_utf8_to_utf16(Str src);
// Returns length of the string in 2-bytes characters
u32 unicode_utf16_strlen(char *str);
