#pragma once
#include "fat.h"

typedef enum ElfLoadStatus
{
	ELF_LOAD_OK,
	ELF_LOAD_FORMAT_ERROR,
	ELF_LOAD_FAT_ERROR,
	ELF_LOAD_MEM_ERROR,
	ELF_LOAD_OUT_OF_MEMORY
} ElfLoadStatus;

typedef struct ElfLoadResult
{
	u32 entry_point;
	ElfLoadStatus status;
} ElfLoadResult;

ElfLoadResult elf_load_kernel(FatFile *fp);
