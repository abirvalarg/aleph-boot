#pragma once
#include "mmc.h"
#include "types.h"

typedef struct MbrPartitionInfo
{
	u32 first_sector;
	u32 num_sectors;
	u8 status;
	u8 partition_type;
} MbrPartitionInfo;

typedef struct Mbr
{
	Mmc *mmc;
	MbrPartitionInfo partitions[4];
} Mbr;

typedef struct MbrPartition
{
	Mmc *mmc;
	u32 first_sector;
	u32 num_sectors;
} MbrPartition;

typedef enum MbrStatus
{
	MBR_OK,
	MBR_MMC_ERROR,
	MBR_NO_SIGNATURE,
	MBR_OUT_OF_RANGE,
	MBR_OUT_OF_MEMORY
} MbrStatus;

MbrStatus Mbr_init(Mbr *mbr, Mmc *mmc);
MbrPartition Mbr_partition(const Mbr *mbr, u8 partition);
MbrStatus MbrPartition_read_sector(MbrPartition part, u32 sector, u8 *buffer);
