#include "control.h"

#define NUM_CPU_MODELS 7

const char *const cpu_names[8] = {
    [CPU_UNKNOWN] = "<unknown>",
    [CPU_AM3351] = "AM3351",
    [CPU_AM3352] = "AM3352",
    [CPU_AM3354] = "AM3354",
    [CPU_AM3356] = "AM3356",
    [CPU_AM3357] = "AM3357",
    [CPU_AM3358] = "AM3358",
    [CPU_AM3359] = "AM3359",
};

const char *const revision_names[3] = {
    [SIL_REV_10] = "1.0",
    [SIL_REV_20] = "2.0",
    [SIL_REV_21] = "2.1"
};

typedef struct DevFeaturePair
{
    u32 reg_value;
    CPUModel model;
} DevFeaturePair;

static const DevFeaturePair models[NUM_CPU_MODELS] = {
    {0x00FC0302, CPU_AM3351},
    {0x00FC0382, CPU_AM3352},
    {0x20FC0382, CPU_AM3354},
    {0x00FD0383, CPU_AM3356},
    {0x00FF0383, CPU_AM3357},
    {0x20FD0383, CPU_AM3358},
    {0x20FF0383, CPU_AM3359}
};

CrystalClockFreq control_crystap_clock_freq(void)
{
    u32 status = CONTROL[CONTROL_STATUS];
    return status >> 22;
}

u8 control_boot_config(void)
{
	return CONTROL[CONTROL_STATUS] & 0xff;
}

CPUModel control_cpu_model(void)
{
    u32 dev_feature = CONTROL[CONTROL_DEV_FEATURE];
    for(unsigned i = 0; i < NUM_CPU_MODELS; i++)
    {
        if(dev_feature == models[i].reg_value)
            return models[i].model;
    }
    return CPU_UNKNOWN;
}

SiliconRevision control_silicon_revision(void)
{
    return CONTROL[CONTROL_DEVICE_ID] >> 28;
}

void control_config_vtp(void)
{
    CONTROL[CONTROL_VTP_CTRL] |= 1 << 6;
    CONTROL[CONTROL_VTP_CTRL] &= ~1;
    CONTROL[CONTROL_VTP_CTRL] |= 1;

    while((CONTROL[CONTROL_VTP_CTRL] & (1 << 5)) == 0);
}

void control_config_ddr(void)
{
    CONTROL[CONTROL_DDR_CMD0_IOCTRL] = 0x18b;
    CONTROL[CONTROL_DDR_CMD1_IOCTRL] = 0x18b;
    CONTROL[CONTROL_DDR_CMD2_IOCTRL] = 0x18b;
    CONTROL[0x1410 / 4] = 0x18b;    // undocumented
    // not setting ddr_data0_ioctrl?
    CONTROL[CONTROL_DDR_DATA1_IOCTRL] = 0x18b;
    CONTROL[0x1448 / 4] = 0x18b;    // undocumented
}

void control_pin_mode(ControlPin pin, u32 mode)
{
    CONTROL[pin] = mode & 0b1111111;
}
