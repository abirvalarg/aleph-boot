#include "heap.h"
#include "uart.h"

#define FREE 0
#define TAKEN 1
#define LAST 2
#define STATUS_MASK 3

extern u32 HEAP_START, HEAP_END;

static u32 *first_free;

/* Headers formats:
 * 	free block:
 * 		[31:2] = length / 4
 * 		[1:0] = "00"
 * 	taken block:
 * 		[31:2] = length / 4
 * 		[1:0] = "01"
 * 	last:
 * 		[31:2] = <don't care>
 * 		[1:0] = "10"
 */

#pragma GCC diagnostic ignored "-Warray-bounds"
void heap_init(void)
{
	u32 length = ((u32)&HEAP_END - (u32)&HEAP_START) / 4 - 2;
	HEAP_START = length << 2;
	u32 *last = &HEAP_END - 1;
	*last = LAST;
	first_free = &HEAP_START;
}

void *malloc(u32 size)
{
	u32 padding = size % 4 == 0 ? 0 : 1;
	size = size / 4 + padding;
	if(size == 0)
		size = 1;

	u32 *block = first_free;
	u8 skipped_free = 0;
	while((*block & STATUS_MASK) != LAST)
	{
		u32 length = *block >> 2;
		u32 status = *block & STATUS_MASK;
		u32 *next = block + length + 1;
		if(status == FREE && length >= size)
		{
			next = block + size + 1;
			if(size < length)
				*next = (length - size - 1) << 2;
			if(!skipped_free)
				first_free = next;
			*block = size << 2 | TAKEN;
			void *result = block + 1;
			return result;
		}
		else if(status == FREE && (*next & STATUS_MASK) == FREE)	// next block is free, concatenating
		{
			u32 new_length = length + (*next >> 2) + 1;
			*block = new_length << 2 | FREE;
			continue;
		}
		else if(status == FREE)
			skipped_free = 1;
		block = next;
	}
	return 0;
}

void free(void *_ptr)
{
	u32 *ptr = _ptr;
	u32 *header = ptr - 1;
	*header = (*header & ~STATUS_MASK) | FREE;
	u32 *next = header + (*header >> 2) + 1;
	if((*next & STATUS_MASK) == FREE)
		*header = ((*header >> 2) + (*next >> 2) + 1) << 2;
	if(header < first_free)
		first_free = header;
}
