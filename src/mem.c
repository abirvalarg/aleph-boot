#include <stddef.h>
#include "types.h"

void *memset(void *dest, int ch, size_t count)
{
	u8 *dest_u8 = dest;
	u8 value = ch;
	for(size_t pos = 0; pos < count; pos++)
		dest_u8[pos] = value;
	return dest;
}

void *memcpy(void *restrict _dest, const void *restrict _src, size_t count)
{
	u8 *dest = _dest;
	const u8 *src = _src;
	for(size_t pos = 0; pos < count; pos++)
		dest[pos] = src[pos];
	return dest;
}

int memory_equal(u8 *a, u8 *b, u32 length)
{
	for(u32 pos = 0; pos < length; pos++)
		if(a[pos] != b[pos])
			return 0;
	return 1;
}
