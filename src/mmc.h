#pragma once
#include "types.h"

typedef volatile u32 MmcRegs;

typedef enum MmcState
{
	MMC_ST_UNKNOWN,			// Initial state is unknown
	MMC_ST_RESET,			// Module was reset
	MMC_ST_INITIALIZING,	// Waiting for card initialization
	MMC_ST_CARD_RESET,		// Resetting the card
	MMC_ST_CHECK_SDIO,		// Checking for SDIO card
	MMC_ST_CHECK_VOLTAGES,	// Checking supported voltages
	MMC_ST_CHECK_SD_PREP,	// Preparing to send ACMD41 (host capacity support)
	MMC_ST_CHECK_SD,		// Checking for SD card and it's capacity (ACMD41)
	MMC_ST_SD_GET_CID,		// Waiting for CID register that i don't really need
	MMC_ST_SD_GET_REL_ADDR,	// Waiting for the card to send it's relative address
	MMC_ST_GET_CSD,			// Waiting for CSD register
	MMC_ST_READY,			// Card is identified and ready to work
	MMC_ST_SELECT,
	MMC_ST_READING,
	MMC_ST_FAILED,			// Failed to initialize or identify the card
} MmcState;

typedef enum MmcStatus
{
	MMC_OK,
	MMC_PROBLEM_WITH_VOLTAGE,
	MMC_UNSUPPORTED_DEVICE,
	MMC_OUT_OF_MEMORY,
	MMC_OUT_OF_RANGE
} MmcStatus;

typedef enum MmcDeviceKind
{
	MMC_DEV_UNKNOWN,
	MMC_DEV_EMPTY,
	MMC_DEV_MMC,
	MMC_DEV_SD,
	MMC_DEV_SDIO
} MmcDeviceKind;

typedef struct Mmc
{
	MmcRegs *regs;
	u32 *read_buffer;
	u32 physical_block_in_buffer;
	// physical block size in number of 512-byte blocks
	volatile u32 block_size;
	u32 num_blocks;
	volatile MmcState state;
	volatile MmcDeviceKind dev_kind;
	u16 rca;
	u8 is_embedded : 1;
	u8 is_sdsc : 1;		// standard capacity
	u8 chs : 1;
	u8 read_buffer_is_valid : 1;
} Mmc;

typedef struct MmcVoltageInfo
{
	u8 v18;
	u8 v30;
	u8 v33;
} MmcVoltageInfo;

void mmc_reset(Mmc *mmc);
void mmc_set_voltages(Mmc *mmc, MmcVoltageInfo vs);
MmcStatus mmc_init(Mmc *mmc, u8 reverse_card_detect_polarity);
void mmc_deinit(Mmc *mmc);
void mmc_deselect_all(Mmc *mmc);
void mmc_select_the_only_card(Mmc *mmc);
// Read a 512-byte logical block
MmcStatus mmc_read_block(Mmc *mmc, u32 block_number, u8 *buffer);
void mmc_on_irq(Mmc *mmc);

extern Mmc MMC0;
extern Mmc MMC1;

#define MMC0_REGS ((MmcRegs*)0x48060000)
#define MMC1_REGS ((MmcRegs*)0x481d8000)

#define SD_SYSCONFIG (0x110 / 4)
#define SD_SYSSTATUS (0x114 / 4)
#define SD_CON (0x12c / 4)
#define SD_BLK (0x204 / 4)
#define SD_ARG (0x208 / 4)
#define SD_CMD (0x20c / 4)
#define SD_RSP10 (0x210 / 4)
#define SD_RSP32 (0x214 / 4)
#define SD_RSP54 (0x218 / 4)
#define SD_RSP76 (0x21c / 4)
#define SD_DATA (0x220 / 4)
#define SD_PSTATE (0x224 / 4)
#define SD_HCTL (0x228 / 4)
#define SD_SYSCTL (0x22c / 4)
#define SD_STAT (0x230 / 4)
#define SD_IE (0x234 / 4)
#define SD_ISE (0x238 / 4)
#define SD_CAPA (0x240 / 4)

// Card detect polarity
#define SD_CON_CDP (1 << 7)
#define SD_CON_DW8 (1 << 5)
#define SD_CON_INIT (1 << 1)

#define SD_CMD_INDEX_SHIFT 24
#define SD_CMD_DATA_PRESENT (1 << 21)
#define SD_CMD_RESP_TYPE_NONE 0
#define SD_CMD_RESP_TYPE_136_BITS (1 << 16)
#define SD_CMD_RESP_TYPE_48_BITS (2 << 16)
#define SD_CMD_RESP_TYPE_48_BITS_WITH_BUSY (3 << 16)
#define SD_CMD_DDIR_WRITE 0
#define SD_CMD_DDIR_READ (1 << 4)

#define SD_PSTATE_CDPL (1 << 18)
#define SD_PSTATE_CSS (1 << 17)
#define SD_PSTATE_CINS (1 << 16)
#define SD_PSTATE_BRE (1 << 11)
#define SD_PSTATE_CMDI 1

#define SD_SVDS_18V (5 << 9)
#define SD_SVDS_30V (6 << 9)
#define SD_SVDS_33V (7 << 9)

#define SD_SDBP (1 << 8)

#define SD_HCTL_DTW (1 << 1)

#define SD_SYSCTL_CLKD_SHIFT 6

#define SD_SYSCTL_SRD (1 << 26)
#define SD_SYSCTL_SRC (1 << 25)
#define SD_SYSCTL_SRA (1 << 24)
#define SD_SYSCTL_CEN (1 << 2)
#define SD_SYSCTL_ICS (1 << 1)
#define SD_SYSCTL_ICE 1

#define SD_STAT_CTO (1 << 16)
#define SD_STAT_CC 1
