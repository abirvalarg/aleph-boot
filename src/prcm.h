#pragma once
#include "types.h"

typedef struct PrcmRegs
{
	volatile u32 *cm_per;
	volatile u32 *cm_wkup;
} PrcmRegs;

typedef enum PrcmModuleMode
{
	PRCM_MODE_DISABLED = 0,
	PRCM_MODE_ENABLED = 2
} PrcmModuleMode;

typedef enum PrcmModuleIdleStatus
{
	PRCM_IDLEST_FUNCTIONAL = 0,
	PRCM_IDLEST_TRANSITION = 1,
	PRCM_IDLEST_IDLE = 2,
	PRCM_IDLEST_DISABLED = 3
} PrcmModuleIdleStatus;

typedef enum PrcmWkupModule
{
	PRCM_WKUP_MODULE_CONTROL = 0x4 / 4,
	PRCM_WKUP_MODULE_GPIO0 = 0x8 / 4,
	PRCM_WKUP_MODULE_UART0 = 0xb4 / 4,
	PRCM_WKUP_MODULE_I2C0 = 0xb8 / 4
} PrcmWkupModule;

typedef enum PrcmPerModule
{
	PRCM_PER_MODULE_EMIF = 0x28 / 4,
	PRCM_PER_MODULE_MMC0 = 0x3c / 4,
	PRCM_PER_MODULE_GPIO1 = 0xac / 4,
	PRCM_PER_MODULE_EMIF_FW = 0xd0 / 4,  // undocumented?
	PRCM_PER_MODULE_MMC1 = 0xf4 / 4
} PrcmPerModule;

typedef struct PllConfig
{
	u16 m;
	u8 n;
	s8 m2;
	u8 sd;
} PllConfig;

void prcm_config_core_pll(const PllConfig *cfg, u8 m4, u8 m5, u8 m6);
void prcm_config_peripheral_pll(const PllConfig *cfg);
void prcm_config_mpu_pll(const PllConfig *cfg);
void prcm_config_ddr_pll(const PllConfig *cfg);

void prcm_wkup_switch(PrcmWkupModule module, PrcmModuleMode mode);
void prcm_per_switch(PrcmPerModule module, PrcmModuleMode mode);
PrcmModuleIdleStatus prcm_wkup_module_idle_status(PrcmWkupModule module);

#define PRCM_CM_IDLEST_DPLL_MPU (0x20 / 4)
#define PRCM_CM_CLKSEL_DPLL_MPU (0x2c / 4)
#define PRCM_CM_IDLEST_DPLL_DDR (0x34 / 4)
#define PRCM_CM_CLKSEL_DPLL_DDR (0x40 / 4)
#define PRCM_CM_IDLEST_DPLL_CORE (0x5c / 4)
#define PRCM_CM_CLKSEL_DPLL_CORE (0x68 / 4)
#define PRCM_CM_IDLEST_DPLL_PER (0x70 / 4)
#define PRCM_CM_DIV_M4_DPLL_CORE (0x80 / 4)
#define PRCM_CM_CLKMODE_DPLL_PER (0x8c / 4)
#define PRCM_CM_DIV_M5_DPLL_CORE (0x84 / 4)
#define PRCM_CM_CLKMODE_DPLL_MPU (0x88 / 4)
#define PRCM_CM_CLKMODE_DPLL_CORE (0x90 / 4)
#define PRCM_CM_CLKMODE_DPLL_DDR (0x94 / 4)
#define PRCM_CM_CLKSEL_DPLL_PER (0x9c / 4)
#define PRCM_CM_DIV_M2_DPLL_DDR (0xa0 / 4)
#define PRCM_CM_DIV_M2_DPLL_MPU (0xa8 / 4)
#define PRCM_CM_DIV_M2_DPLL_PER (0xac / 4)
#define PRCM_CM_DIV_M6_DPLL_CORE (0xd8 / 4)

// CM_CLKSEL_DPLL_*
#define PRCM_DPLL_MULT_SHIFT 8
#define PRCM_DPLL_MULT_MASK (0b11111111111 << PRCM_DPLL_MULT_SHIFT)

#define PRCM_DPLL_DIV_SHIFT 0
#define PRCM_DPLL_DIV_MASK (0b1111111 << PRCM_DPLL_DIV_SHIFT)

// CM_CLKMODE_DPLL_*
#define PRCM_DPLL_EN_SHIFT 0
#define PRCM_DPLL_EN_MASK (0b111 << PRCM_DPLL_EN_SHIFT)

#define PLL_MODE_BYPASS 4
#define PLL_MODE_LOCK 7

// CM_IDLEST_DPLL_*
#define PLL_ST_DPLL_CLK 1
#define PLL_ST_MN_BYPASS_MASK (1 << 8)

// CM_DIV_M2_DPLL_*
#define DIV_M2_CLKOUT_DIV_MASK 0b11111

extern PrcmRegs prcm;
