#include "mmc.h"
#include "uart.h"
#include "irq.h"
#include "heap.h"

#define AS_STR(x) AS_STR_(x)
#define AS_STR_(x) #x

static int card_detect(Mmc *mmc);
static void decode_mmc_csd_from_response(Mmc *mmc);
static void decode_sd_csd_from_response(Mmc *mmc);

void mmc_reset(Mmc *mmc)
{
	mmc->regs[SD_SYSCONFIG] = 1 << 1;
	while((mmc->regs[SD_SYSSTATUS] & 1) == 0);
	mmc->state = MMC_ST_RESET;
}

void mmc_set_voltages(Mmc *mmc, MmcVoltageInfo vs)
{
	u32 capabilities = mmc->regs[SD_CAPA] & ~(0b111 << 24);
	if(vs.v18)
		capabilities |= 1 << 26;
	if(vs.v30)
		capabilities |= 1 << 25;
	if(vs.v33)
		capabilities |= 1 << 24;
	mmc->regs[SD_CAPA] = capabilities;
}

MmcStatus mmc_init(Mmc *mmc, u8 reverse_card_detect_polarity)
{
	mmc->regs[SD_CON] = (reverse_card_detect_polarity ? 1 << 7 : 0);

	u32 capabilities = mmc->regs[SD_CAPA];
	u32 voltage_select;
	if(capabilities & 1 << 24)
		voltage_select = SD_SVDS_33V;
	else if(capabilities & 1 << 25)
		voltage_select = SD_SVDS_30V;
	else if(capabilities & 1 << 26)
		voltage_select = SD_SVDS_18V;
	else
		return MMC_PROBLEM_WITH_VOLTAGE;
	mmc->regs[SD_HCTL] = voltage_select;
	mmc->regs[SD_SYSCTL] = 0x3ff << SD_SYSCTL_CLKD_SHIFT | SD_SYSCTL_ICE;
	while((mmc->regs[SD_SYSCTL] & SD_SYSCTL_ICS) == 0);
	mmc->regs[SD_SYSCTL] |= SD_SYSCTL_CEN;
	mmc->regs[SD_HCTL] = voltage_select | SD_SDBP;
	while((mmc->regs[SD_HCTL] & SD_SDBP) == 0);
	if(mmc->is_embedded || card_detect(mmc))
	{
		mmc->regs[SD_IE] = 1 << 16 | 1;
		mmc->regs[SD_ISE] = 1 << 16 | 1;

		mmc->state = MMC_ST_INITIALIZING;

		mmc->regs[SD_SYSCTL] |= SD_SYSCTL_SRC;
		while((mmc->regs[SD_SYSCTL] & SD_SYSCTL_SRC) == 0);
		while(mmc->regs[SD_SYSCTL] & SD_SYSCTL_SRC);

		mmc->regs[SD_CON] |= SD_CON_INIT;
		mmc->regs[SD_CMD] = 0;
	}
	else
		mmc->dev_kind = MMC_DEV_EMPTY;
	while(mmc->state != MMC_ST_READY && mmc->state != MMC_ST_FAILED);
	if(mmc->state == MMC_ST_READY)
	{
		if(mmc->dev_kind == MMC_DEV_SD)
		{
			mmc->read_buffer = malloc(mmc->block_size * 512);
			if(mmc->read_buffer)
				return MMC_OK;
			else
				return MMC_OUT_OF_MEMORY;
		}
	}
	return MMC_UNSUPPORTED_DEVICE;
}

void mmc_deinit(Mmc *mmc)
{
	if(mmc->read_buffer)
		free(mmc->read_buffer);
	mmc->read_buffer = 0;
	mmc->read_buffer_is_valid = 0;
}

void mmc_deselect_all(Mmc *mmc)
{
	while(mmc->state != MMC_ST_READY);
	mmc->state = MMC_ST_SELECT;
	mmc->regs[SD_ARG] = 0;
	mmc->regs[SD_CMD] = 7 << SD_CMD_INDEX_SHIFT | SD_CMD_RESP_TYPE_NONE;
	while(mmc->state == MMC_ST_SELECT);
}

void mmc_select_the_only_card(Mmc *mmc)
{
	while(mmc->state != MMC_ST_READY);
	mmc->state = MMC_ST_SELECT;
	mmc->regs[SD_ARG] = mmc->rca << 16;
	mmc->regs[SD_CMD] = 7 << SD_CMD_INDEX_SHIFT | SD_CMD_RESP_TYPE_48_BITS_WITH_BUSY;
	while(mmc->state == MMC_ST_SELECT);
}

MmcStatus mmc_read_block(Mmc *mmc, u32 block_number, u8 *buffer)
{
	if(!mmc->read_buffer)
		return MMC_OUT_OF_MEMORY;
	u32 phys_block_number = block_number / mmc->block_size;
	if(phys_block_number >= mmc->num_blocks)
		return MMC_OUT_OF_RANGE;

	if(!mmc->read_buffer_is_valid || mmc->physical_block_in_buffer != phys_block_number)
	{
		while(mmc->state != MMC_ST_READY);
		mmc->state = MMC_ST_READING;
		if(mmc->is_sdsc)
			mmc->regs[SD_ARG] = phys_block_number * mmc->block_size * 512;
		else
			mmc->regs[SD_ARG] = phys_block_number;
		mmc->regs[SD_CMD] = 17 << SD_CMD_INDEX_SHIFT | SD_CMD_RESP_TYPE_48_BITS | SD_CMD_DATA_PRESENT | SD_CMD_DDIR_READ;
		while(mmc->state == MMC_ST_READING);

		for(unsigned pos = 0; pos * 4 < mmc->block_size * 512; pos++)
		{
			while((mmc->regs[SD_PSTATE] & SD_PSTATE_BRE) == 0);
			mmc->read_buffer[pos] = mmc->regs[SD_DATA];
		}

		mmc->physical_block_in_buffer = phys_block_number;
		mmc->read_buffer_is_valid = 1;
	}

	u32 logical_block_offset = (block_number % mmc->block_size) * 512;
	u8 *read_buffer = (u8*)mmc->read_buffer;

	for(unsigned pos = 0; pos < 512; pos++)
		buffer[pos] = read_buffer[pos + logical_block_offset];
	return MMC_OK;
}

void mmc_on_irq(Mmc *mmc)
{
	u32 stat = mmc->regs[SD_STAT];
	if(stat & SD_STAT_CTO)
	{
		mmc->regs[SD_STAT] = SD_STAT_CTO;
		switch(mmc->state)
		{
		case MMC_ST_CHECK_SDIO:
			// not SDIO
			mmc->regs[SD_SYSCTL] |= SD_SYSCTL_SRC;
			while((mmc->regs[SD_SYSCTL] & SD_SYSCTL_SRC) == 0);
			while(mmc->regs[SD_SYSCTL] & SD_SYSCTL_SRC);
			mmc->state = MMC_ST_CHECK_VOLTAGES;
			mmc->regs[SD_ARG] = 0b0001 << 8;
			mmc->regs[SD_CMD] = 8 << 24 | 2 << 16;
			break;

		case MMC_ST_CHECK_VOLTAGES:
			// MMC or SD 1.x
			mmc->regs[SD_SYSCTL] |= SD_SYSCTL_SRC;
			while((mmc->regs[SD_SYSCTL] & SD_SYSCTL_SRC) == 0);
			while(mmc->regs[SD_SYSCTL] & SD_SYSCTL_SRC);
			mmc->state = MMC_ST_CHECK_SD_PREP;
			mmc->chs = 0;
			mmc->regs[SD_ARG] = 0;
			mmc->regs[SD_CMD] = 55 << 24 | 2 << 16;
			break;

		case MMC_ST_CHECK_SD_PREP:
			// MMC
			mmc->dev_kind = MMC_DEV_MMC;
			mmc->regs[SD_CON] |= SD_CON_DW8;
			mmc->state = MMC_ST_READY;
			break;

		case MMC_ST_CHECK_SD:
			// Incompatible voltages
			mmc->state = MMC_ST_FAILED;
			break;

		default:
			mmc->state = MMC_ST_FAILED;
			break;
		}
	}
	else if(stat & SD_STAT_CC)
	{
		mmc->regs[SD_STAT] = SD_STAT_CC;
		u32 ocr;
		switch(mmc->state)
		{
		case MMC_ST_INITIALIZING:
			mmc->regs[SD_CON] &= ~SD_CON_INIT;
			mmc->regs[SD_SYSCTL] = (mmc->regs[SD_SYSCTL] & ~(0x3ff << 6 | SD_SYSCTL_CEN)) | 250 << SD_SYSCTL_CLKD_SHIFT;
			while((mmc->regs[SD_SYSCTL] & SD_SYSCTL_ICS) == 0);
			mmc->regs[SD_SYSCTL] |= SD_SYSCTL_CEN;
			while(mmc->regs[SD_PSTATE] & SD_PSTATE_CMDI);
			mmc->regs[SD_ARG] = 0;
			mmc->regs[SD_CMD] = 0;	// reset
			mmc->state = MMC_ST_CARD_RESET;
			break;

		case MMC_ST_CARD_RESET:
			mmc->state = MMC_ST_CHECK_SDIO;
			mmc->regs[SD_CMD] = 5 << 24 | 2 << 16;
			break;

		case MMC_ST_CHECK_SDIO:
			mmc->dev_kind = MMC_DEV_SDIO;
			mmc->state = MMC_ST_READY;
			break;

		case MMC_ST_CHECK_VOLTAGES:
			mmc->state = MMC_ST_CHECK_SD_PREP;
			mmc->chs = 1;
			// prepare to send ACMD
			mmc->regs[SD_ARG] = 0;
			mmc->regs[SD_CMD] = 55 << SD_CMD_INDEX_SHIFT | SD_CMD_RESP_TYPE_48_BITS;
			break;

		case MMC_ST_CHECK_SD_PREP:
			mmc->state = MMC_ST_CHECK_SD;
			// ACMD41 (operation conditions)
			mmc->regs[SD_ARG] = (mmc->chs ? 1 << 30 : 0) | 1 << 28 | 1 << 21;
			mmc->regs[SD_CMD] = 41 << SD_CMD_INDEX_SHIFT | SD_CMD_RESP_TYPE_48_BITS;
			break;

		case MMC_ST_CHECK_SD:
			mmc->dev_kind = MMC_DEV_SD;
			// mmc->regs[SD_HCTL] |= SD_HCTL_DTW;
			ocr = mmc->regs[SD_RSP10];
			if(ocr & 1 << 31)	// ready
			{
				mmc->is_sdsc = (ocr & 1 << 30) == 0;
				mmc->state = MMC_ST_SD_GET_CID;
				// Send CID
				mmc->regs[SD_ARG] = 0;
				mmc->regs[SD_CMD] = 2 << SD_CMD_INDEX_SHIFT | SD_CMD_RESP_TYPE_136_BITS;
			}
			else
			{
				mmc->state = MMC_ST_CHECK_SD_PREP;
				// try again
				mmc->regs[SD_ARG] = 0;
				mmc->regs[SD_CMD] = 55 << SD_CMD_INDEX_SHIFT | SD_CMD_RESP_TYPE_48_BITS;
			}
			break;

		case MMC_ST_SD_GET_CID:
			mmc->state = MMC_ST_SD_GET_REL_ADDR;
			// Send new RCA
			mmc->regs[SD_ARG] = 0;
			mmc->regs[SD_CMD] = 3 << SD_CMD_INDEX_SHIFT | SD_CMD_RESP_TYPE_48_BITS;
			break;

		case MMC_ST_SD_GET_REL_ADDR:
			mmc->rca = mmc->regs[SD_RSP10] >> 16;
			mmc->state = MMC_ST_GET_CSD;
			// Get card specific data to determine capacity
			mmc->regs[SD_ARG] = (u32)mmc->rca << 16;
			mmc->regs[SD_CMD] = 9 << SD_CMD_INDEX_SHIFT | SD_CMD_RESP_TYPE_136_BITS;
			break;

		case MMC_ST_GET_CSD:
			switch(mmc->dev_kind)
			{
			case MMC_DEV_MMC:
				decode_mmc_csd_from_response(mmc);
				break;

			case MMC_DEV_SD:
				decode_sd_csd_from_response(mmc);
				break;

			default:
				mmc->state = MMC_ST_FAILED;
				break;
			}
			break;

		case MMC_ST_SELECT:
		case MMC_ST_READING:
			mmc->state = MMC_ST_READY;
			break;

		default:
			mmc->state = MMC_ST_FAILED;
			break;
		}
	}
}

static int card_detect(Mmc *mmc)
{
	while((mmc->regs[SD_PSTATE] & SD_PSTATE_CSS) == 0);
	u8 reverse_card_detect_polarity = (mmc->regs[SD_CON] & SD_CON_CDP) != 0;
	u8 card_detect = (mmc->regs[SD_PSTATE] & SD_PSTATE_CINS) != 0;
	return card_detect != reverse_card_detect_polarity;
}

static void decode_sd_csd_from_response(Mmc *mmc)
{
	// u32 word0 = mmc->regs[SD_RSP10];
	u32 word1 = mmc->regs[SD_RSP32];
	u32 word2 = mmc->regs[SD_RSP54];
	u32 word3 = mmc->regs[SD_RSP76];
	switch(word3 >> 30)
	{
	case 0:
		// SDSC CSD
		// need fields:
		// C_SIZE [73:62]	word2[9:0] & word1[31:30]
		// C_SIZE_MULT [49:47]	word1[17:15]
		// READ_BL_LEN [83:80]	word2[19:16]
		(void)0;
		u32 c_size = (word2 & 0x3ff) << 2 | (word1 >> 30);
		u8 c_size_mult = (word1 >> 15) & 0b111;
		u8 read_bl_len = (word2 >> 16) & 0b1111;
		u32 mult = 1 << (c_size_mult + 2);
		u32 block_size = 1 << read_bl_len;
		mmc->block_size = block_size / 512;
		mmc->regs[SD_BLK] = block_size;
		mmc->num_blocks = (c_size + 1) * mult;
		mmc->state = MMC_ST_READY;
		break;

	case 1:
		// SDHC or SDXC CSD
		// C_SIZE [69:48]	word2[5:0] & word1[31:16]
		c_size = (word2 & 0x3f) << 16 | (word1 >> 16);
		mmc->block_size = 1;
		mmc->regs[SD_BLK] = 512;
		mmc->num_blocks = (c_size + 1) * 1024;
		mmc->state = MMC_ST_READY;
		break;

	default:
		mmc->state = MMC_ST_FAILED;
		break;
	}
}

static void decode_mmc_csd_from_response(Mmc *mmc)
{
	mmc->state = MMC_ST_FAILED;
}

Mmc MMC0 = {
	.regs = MMC0_REGS,
	.state = MMC_ST_UNKNOWN,
	.dev_kind = MMC_DEV_UNKNOWN,
	.is_embedded = 0
};

Mmc MMC1 = {
	.regs = MMC1_REGS,
	.state = MMC_ST_UNKNOWN,
	.dev_kind = MMC_DEV_UNKNOWN,
	.is_embedded = 0
};
