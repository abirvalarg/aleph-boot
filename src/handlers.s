.syntax unified

.section .text.asm_start
.global asm_start
asm_start:
	cpsid if, 0b10010	// IRQ mode
	ldr sp, =0x4030CE00

	cps 0b11111	// system mode
	ldr sp, =0x4030CE00 - 3 * 1024

	ldr r0, =vectors
	mcr p15, 0, r0, c12, c0, 0
	cpsie a

	b start

.section .text.asm_on_irq
.global asm_on_irq
asm_on_irq:
	push {r0-r3, lr}
	bl on_irq
	pop {r0-r3, lr}
	subs pc, lr, #4

.section .text.asm_undefined_instruction
.global asm_undefined_instruction
asm_undefined_instruction:
	ldr sp, =0x4030CE00

	push {lr}
	mrs r0, spsr
	and r0, 1 << 5
	bl undefined_instruction
	pop {lr}

	ands r1, 1 << 5
	// ARM: -4
	subeq r0, lr, 4
	// Thumb: -2
	subne r0, lr, 2

	bl dump_previous_pc
	b .

.section .text.asm_prefetch_abort
.global asm_prefetch_abort
asm_prefetch_abort:
	ldr sp, =0x4030CE00
	push {lr}
	push {r0-r3}
	bl prefetch_abort
	pop {r0-r3}
	bl dump_regular_registers
	pop {lr}
	sub r0, lr, 4
	bl dump_previous_pc
	b .

.section .text.asm_data_abort
.global asm_data_abort
asm_data_abort:
	ldr sp, =0x4030CE00
	push {lr}
	push {r0-r3}
	bl data_abort
	pop {r0-r3}
	bl dump_regular_registers

	cps 0b11111
	mov r0, sp
	mov r1, lr
	cps 0b10111
	bl dump_user_sp_and_lr

	pop {lr}
	sub r0, lr, 8
	bl dump_previous_pc
	b .
