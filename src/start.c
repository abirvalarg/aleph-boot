#include "elf.h"
#include "emif.h"
#include "prcm.h"
#include "control.h"
#include "string.h"
#include "uart.h"
#include "gpio.h"
#include "ddr.h"
#include "intc.h"
#include "mmc.h"
#include "mbr.h"
#include "fat.h"
#include "heap.h"
#include "vmem.h"

typedef struct AlephBootInfo
{
	const char *devlist;
	u32 devlist_length;
	const char *config;
	u32 config_length;
	u32 phys_memory_size;
} AlephBootInfo;

static const PllConfig core_pll_configs[4] = {
	[CR_CLOCK_FREQ_19_2] = {625, 11, -1, 0},
	[CR_CLOCK_FREQ_24] = {125, 2, -1, 0},
	[CR_CLOCK_FREQ_25] = {40, 0, -1, 0},
	[CR_CLOCK_FREQ_26] = {500, 12, -1, 0}
};

static const PllConfig per_pll_configs[4] = {
	[CR_CLOCK_FREQ_19_2] = {400, 7, 5, 4},
	[CR_CLOCK_FREQ_24] = {400, 9, 5, 4},
	[CR_CLOCK_FREQ_25] = {384, 9, 5, 4},
	[CR_CLOCK_FREQ_26] = {480, 12, 5, 4}
};

static const PllConfig mpu_pll_configs[4] = {
	[CR_CLOCK_FREQ_19_2] = {125, 3, 1, 0},
	[CR_CLOCK_FREQ_24] = {25, 0, 1, 0},
	[CR_CLOCK_FREQ_25] = {24, 0, 1, 0},
	[CR_CLOCK_FREQ_26] = {300, 12, 1, 0}
};

static const PllConfig ddr_pll_configs[4] = {
	[CR_CLOCK_FREQ_19_2] = {125, 5, 1, 0},
	[CR_CLOCK_FREQ_24] = {50, 2, 1, 0},
	[CR_CLOCK_FREQ_25] = {16, 0, 1, 0},
	[CR_CLOCK_FREQ_26] = {200, 12, 1, 0}
};

static void boot_from_mmc(Mmc *mmc, u8 reverse_card_detect_polarity);

__attribute__((noreturn))
void start(void)
{
	heap_init();

	CrystalClockFreq master_clock_freq = control_crystap_clock_freq();
	prcm_config_core_pll(&core_pll_configs[master_clock_freq], 10, 8, 4);
	prcm_config_peripheral_pll(&per_pll_configs[master_clock_freq]);
	prcm_config_mpu_pll(&mpu_pll_configs[master_clock_freq]);
	prcm_config_ddr_pll(&ddr_pll_configs[master_clock_freq]);

	prcm_wkup_switch(PRCM_WKUP_MODULE_CONTROL, PRCM_MODE_ENABLED);

	// UART0 pins
	control_pin_mode(CONTROL_PIN_UART0_RXD, 0 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	control_pin_mode(CONTROL_PIN_UART0_TXD, 0 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_DISABLE | CONTROL_MODE_SLEW_FAST);

	// GPIO1 pins 20-24
	control_pin_mode(CONTROL_PIN_GPMC_A4, 7 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_DISABLE | CONTROL_MODE_SLEW_SLOW);
	control_pin_mode(CONTROL_PIN_GPMC_A5, 7 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_DISABLE | CONTROL_MODE_SLEW_SLOW);
	control_pin_mode(CONTROL_PIN_GPMC_A6, 7 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_DISABLE | CONTROL_MODE_SLEW_SLOW);
	control_pin_mode(CONTROL_PIN_GPMC_A7, 7 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_DISABLE | CONTROL_MODE_SLEW_SLOW);
	control_pin_mode(CONTROL_PIN_GPMC_A8, 7 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_DISABLE | CONTROL_MODE_SLEW_SLOW);

	prcm_wkup_switch(PRCM_WKUP_MODULE_UART0, PRCM_MODE_ENABLED);
	prcm_per_switch(PRCM_PER_MODULE_GPIO1, PRCM_MODE_ENABLED);

	for(unsigned pin = 20; pin <= 24; pin++)
	{
		gpio_pin_mode(GPIO1, pin, PIN_MODE_OUTPUT);
		if(pin != 20)
			gpio_pin_write(GPIO1, pin, 0);
	}
	gpio_pin_write(GPIO1, 20, 1);

	uart_reset(UART0);
	uart_setup(UART0);
	uart_send_str(UART0, "\r\nHello from aleph-boot\r\n");

	{
		CPUModel cpu = control_cpu_model();
		const char *model_str = cpu_names[cpu];
		uart_send_str(UART0, "CPU: ");
		uart_send_str(UART0, model_str);
		uart_send_str(UART0, "\r\n");
	}

	{
		SiliconRevision rev = control_silicon_revision();
		const char *rev_str = revision_names[rev];
		uart_send_str(UART0, "Silicon revision: ");
		uart_send_str(UART0, rev_str);
		uart_send_str(UART0, "\r\n");
	}

	prcm_per_switch(PRCM_PER_MODULE_EMIF_FW, PRCM_MODE_ENABLED);
	prcm_per_switch(PRCM_PER_MODULE_EMIF, PRCM_MODE_ENABLED);
	control_config_vtp();
	ddr_config_phy();
	control_config_ddr();
	CONTROL[CONTROL_DDR_CKE_CTRL] = 1;
	emif_config_ddr_phy();
	emif_set_timings();
	emif_config_sdram();

	intc_reset();
	intc_unmask(IRQ_MMCSD1);
	intc_unmask(IRQ_MMCSD0);
	asm("cpsie i");

	u8 boot_order_cfg = control_boot_config() & 0x1f;

	// mmc0 pins
	control_pin_mode(CONTROL_PIN_MMC0_DAT3, 0 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	control_pin_mode(CONTROL_PIN_MMC0_DAT2, 0 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	control_pin_mode(CONTROL_PIN_MMC0_DAT1, 0 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	control_pin_mode(CONTROL_PIN_MMC0_DAT0, 0 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	control_pin_mode(CONTROL_PIN_MMC0_CLK, 0 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	control_pin_mode(CONTROL_PIN_MMC0_CMD, 0 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	// MMC0_SDCD
	control_pin_mode(CONTROL_PIN_SPI0_CS1, 5 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_SLOW);

	// mmc1 pins
	// MMC1_DAT0
	control_pin_mode(CONTROL_PIN_GPMC_AD0, 1 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	// MMC1_DAT1
	control_pin_mode(CONTROL_PIN_GPMC_AD1, 1 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	// MMC1_DAT2
	control_pin_mode(CONTROL_PIN_GPMC_AD2, 1 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	// MMC1_DAT3
	control_pin_mode(CONTROL_PIN_GPMC_AD3, 1 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	// MMC1_DAT4
	control_pin_mode(CONTROL_PIN_GPMC_AD4, 1 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	// MMC1_DAT5
	control_pin_mode(CONTROL_PIN_GPMC_AD5, 1 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	// MMC1_DAT6
	control_pin_mode(CONTROL_PIN_GPMC_AD6, 1 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	// MMC1_DAT7
	control_pin_mode(CONTROL_PIN_GPMC_AD7, 1 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	// MMC1_CLK
	control_pin_mode(CONTROL_PIN_GPMC_CSN1, 2 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);
	// MMC1_CMD
	control_pin_mode(CONTROL_PIN_GPMC_CSN2, 2 | CONTROL_MODE_PULL_DISABLE | CONTROL_MODE_INPUT_ENABLE | CONTROL_MODE_SLEW_FAST);

	prcm_per_switch(PRCM_PER_MODULE_MMC0, PRCM_MODE_ENABLED);
	prcm_per_switch(PRCM_PER_MODULE_MMC1, PRCM_MODE_ENABLED);

	static const MmcVoltageInfo mmc_volt_info = {
		.v33 = 1
	};
	mmc_set_voltages(&MMC0, mmc_volt_info);
	mmc_set_voltages(&MMC1, mmc_volt_info);

	vmem_clear_temporary_tables();

	// SRAM0
	VMemStatus map_st = vmem_map_temporary_large_page(0x402f0000, 0x402f0000);
	if(map_st != VMEM_OK)
	{
		uart_send_str(UART0, "Out of temporary 2nd level tables\r\n");
		goto fail;
	}

	// SRAM1
	map_st = vmem_map_temporary_large_page(0x40300000, 0x40300000);
	if(map_st != VMEM_OK)
	{
		uart_send_str(UART0, "Out of temporary 2nd level tables\r\n");
		goto fail;
	}

	// UART0
	map_st = vmem_map_temporary_small_page_device(0x44e09000, 0x44e09000);
	if(map_st != VMEM_OK)
	{
		uart_send_str(UART0, "Out of temporary 2nd level tables\r\n");
		goto fail;
	}

	// GPIO1
	map_st = vmem_map_temporary_small_page_device(0x4804c000, 0x4804c000);
	if(map_st != VMEM_OK)
	{
		uart_send_str(UART0, "Out of temporary 2nd level tables\r\n");
		goto fail;
	}

	// MMC0
	map_st = vmem_map_temporary_small_page_device(0x48060000, 0x48060000);
	if(map_st != VMEM_OK)
	{
		uart_send_str(UART0, "Out of temporary 2nd level tables\r\n");
		goto fail;
	}

	// MMC1
	map_st = vmem_map_temporary_small_page_device(0x481d8000, 0x481d8000);
	if(map_st != VMEM_OK)
	{
		uart_send_str(UART0, "Out of temporary 2nd level tables\r\n");
		goto fail;
	}

	// INTC
	map_st = vmem_map_temporary_small_page_device(0x48200000, 0x48200000);
	if(map_st != VMEM_OK)
	{
		uart_send_str(UART0, "Out of temporary 2nd level tables\r\n");
		goto fail;
	}

	switch(boot_order_cfg)
	{
	case 0b11100:	// MMC1, MMC0
		uart_send_str(UART0, "Booting from MMC1\r\n");
		boot_from_mmc(&MMC1, 0);
		/* fall through */
	case 0b11000:	// MMC0
		uart_send_str(UART0, "Booting from MMC0\r\n");
		boot_from_mmc(&MMC0, 1);
		break;

	default:
		uart_send_str(UART0, "Unknown boot config\r\n");
		break;
	}

fail:
	uart_send_str(UART0, "Failed to boot\r\n");
	u8 led_state = 0;
	while(1)
	{
		led_state = !led_state;
		gpio_pin_write(GPIO1, 24, led_state);
		for(unsigned i = 0; i < 10000000; i++)
			asm("nop");
	}
}

static void boot_from_mmc(Mmc *mmc, u8 reverse_card_detect_polarity)
{
	mmc_reset(mmc);
	MmcStatus mmc_init_status = mmc_init(mmc, reverse_card_detect_polarity);
	switch(mmc_init_status)
	{
	case MMC_OK:
		break;

	case MMC_PROBLEM_WITH_VOLTAGE:
		uart_send_str(UART0, "Voltages are not set up\r\n");
		return;

	case MMC_UNSUPPORTED_DEVICE:
		uart_send_str(UART0, "Device is not supported\r\n");
		return;

	case MMC_OUT_OF_MEMORY:
		uart_send_str(UART0, "Out of memory\r\n");
		return;

	default:
		uart_send_str(UART0, "Unexpected MMC error\r\n");
		return;
	}

	if(mmc->dev_kind == MMC_DEV_EMPTY)
	{
		mmc_deinit(mmc);
		return;
	}

	mmc_deselect_all(mmc);
	mmc_select_the_only_card(mmc);

	Mbr mbr;
	MbrStatus mbr_init_status = Mbr_init(&mbr, mmc);
	switch(mbr_init_status)
	{
	case MBR_OK:
		break;

	case MBR_MMC_ERROR:
		uart_send_str(UART0, "Failed to read MMC\r\n");
		mmc_deinit(mmc);
		return;

	case MBR_NO_SIGNATURE:
		uart_send_str(UART0, "Device doesn't contain a valid MBR\r\n");
		mmc_deinit(mmc);
		return;

	default:
		uart_send_str(UART0, "Unexpected MBR error\r\n");
		mmc_deinit(mmc);
		return;
	}

	u8 part_type = mbr.partitions[0].partition_type;
	if(!(part_type == 1 || part_type == 4 || part_type == 8 || part_type == 0xc || part_type == 0xe
			|| part_type == 0x11))
	{
		uart_send_str(UART0, "First partition must be a FAT file system\r\n");
		mmc_deinit(mmc);
		return;
	}

	MbrPartition partition = Mbr_partition(&mbr, 0);
	Fat fat;
	FatStatus fat_init_status = Fat_init(&fat, partition);
	switch(fat_init_status)
	{
	case FAT_OK:
		break;

	case FAT_OUT_OF_MEMORY:
		uart_send_str(UART0, "Out of memory\r\n");
		mmc_deinit(mmc);
		return;

	case FAT_MBR_ERROR:
		uart_send_str(UART0, "Failed to read MBR partition\r\n");
		mmc_deinit(mmc);
		return;

	case FAT_NOT_IMPLEMENTED:
		uart_send_str(UART0, "FAT12 is not implemented\r\n");
		mmc_deinit(mmc);
		return;

	default:
		uart_send_str(UART0, "Unexpected FAT error\r\n");
		mmc_deinit(mmc);
		return;
	}

	uart_send_str(UART0, "Loading devlist from `/boot/bbb.devlist`\r\n");

	FatDir root = Fat_open_root_dir(&fat);

	FatDirResult boot_dir_res = FatDir_open_dir(&root, Str_from_cstr("boot"));
	if(boot_dir_res.status != FAT_OK)
	{
		switch(boot_dir_res.status)
		{
		case FAT_OUT_OF_MEMORY:
			uart_send_str(UART0, "Out of memory\r\n");
			break;

		case FAT_MBR_ERROR:
			uart_send_str(UART0, "Failed to read MBR partition\r\n");
			break;

		case FAT_WRONG_OBJECT_KIND:
			uart_send_str(UART0, "Attempt to open a file as a directory\r\n");
			break;

		case FAT_NO_OBJECT:
			uart_send_str(UART0, "Directory not found\r\n");
			break;

		case FAT_NOT_IMPLEMENTED:
			uart_send_str(UART0, "long file names are not implemented\r\n");
			break;

		default:
			uart_send_str(UART0, "Unexpected error while opening test directory\r\n");
			break;
		}
		Fat_deinit(&fat);
		mmc_deinit(mmc);
		return;
	}

	FatFileResult devlist_file_res =
		FatDir_open(&boot_dir_res.result, Str_from_cstr("bbb.devlist"));
	if(devlist_file_res.status != FAT_OK)
	{
		switch(devlist_file_res.status)
		{
		case FAT_OUT_OF_MEMORY:
			uart_send_str(UART0, "Out of memory\r\n");
			break;

		case FAT_MBR_ERROR:
			uart_send_str(UART0, "Failed to read MBR partition\r\n");
			break;

		case FAT_WRONG_OBJECT_KIND:
			uart_send_str(UART0, "Attempt to open a directory as a file\r\n");
			break;

		case FAT_NO_OBJECT:
			uart_send_str(UART0, "File not found\r\n");
			break;

		case FAT_NOT_IMPLEMENTED:
			uart_send_str(UART0, "long file names are not implemented\r\n");
			break;

		default:
			uart_send_str(UART0, "Unexpected error while opening test file\r\n");
			break;
		}
		Fat_deinit(&fat);
		mmc_deinit(mmc);
		return;
	}
	FatFile *devlist_fp = devlist_file_res.result;

	u8 *devlist = malloc(devlist_fp->left_on_disk);
	if(!devlist)
	{
		uart_send_str(UART0, "Out of memory\r\n");
		FatFile_close(devlist_fp);
		Fat_deinit(&fat);
		mmc_deinit(mmc);
		return;
	}

	u32 devlist_len = devlist_fp->left_on_disk;
	{
		u32 bytes_read = 0;
		do
		{
			FatFileReadResult res =
				FatFile_read(devlist_fp, devlist + bytes_read, devlist_len - bytes_read);
			if(res.status != FAT_OK)
			{
				uart_send_str(UART0, "Failed to read devlist\r\n");
				free(devlist);
				FatFile_close(devlist_fp);
				return;
			}
			bytes_read += res.length;
		} while(bytes_read < devlist_len);
	}
	FatFile_close(devlist_fp);

	uart_send_str(UART0, "Loading config from `/boot/aleph.cfg`\r\n");

	FatFileResult config_fp_res = FatDir_open(&boot_dir_res.result, Str_from_cstr("aleph.cfg"));
	if(config_fp_res.status != FAT_OK)
	{
		switch(config_fp_res.status)
		{
		case FAT_OUT_OF_MEMORY:
			uart_send_str(UART0, "Out of memory\r\n");
			break;

		case FAT_MBR_ERROR:
			uart_send_str(UART0, "Failed to read MBR partition\r\n");
			break;

		case FAT_WRONG_OBJECT_KIND:
			uart_send_str(UART0, "Attempt to open a directory as a file\r\n");
			break;

		case FAT_NO_OBJECT:
			uart_send_str(UART0, "File not found\r\n");
			break;

		case FAT_NOT_IMPLEMENTED:
			uart_send_str(UART0, "long file names are not implemented\r\n");
			break;

		default:
			uart_send_str(UART0, "Unexpected error while opening test file\r\n");
			break;
		}
		free(devlist);
		Fat_deinit(&fat);
		mmc_deinit(mmc);
		return;
	}

	FatFile *config_fp = config_fp_res.result;
	u8 *config = malloc(config_fp->left_on_disk);
	if(!config)
	{
		uart_send_str(UART0, "Out of memory\r\n");
		free(devlist);
		FatFile_close(config_fp);
		Fat_deinit(&fat);
		mmc_deinit(mmc);
		return;
	}

	u32 config_len = config_fp->left_on_disk;
	{
		u32 bytes_read = 0;
		do
		{
			FatFileReadResult res =
				FatFile_read(config_fp, config + bytes_read, config_len - bytes_read);
			if(res.status != FAT_OK)
			{
				uart_send_str(UART0, "Failed to read config\r\n");
				free(devlist);
				free(config);
				FatFile_close(config_fp);
				return;
			}
			bytes_read += res.length;
		} while(bytes_read < config_len);
	}
	FatFile_close(config_fp);

	uart_send_str(UART0, "Loading kernel from `/boot/aleph`\r\n");
	FatFileResult aleph_res = FatDir_open(&boot_dir_res.result, Str_from_cstr("aleph"));
	if(aleph_res.status != FAT_OK)
	{
		switch(aleph_res.status)
		{
		case FAT_OUT_OF_MEMORY:
			uart_send_str(UART0, "Out of memory\r\n");
			break;

		case FAT_MBR_ERROR:
			uart_send_str(UART0, "Failed to read MBR partition\r\n");
			break;

		case FAT_WRONG_OBJECT_KIND:
			uart_send_str(UART0, "Attempt to open a directory as a file\r\n");
			break;

		case FAT_NO_OBJECT:
			uart_send_str(UART0, "File not found\r\n");
			break;

		case FAT_NOT_IMPLEMENTED:
			uart_send_str(UART0, "long file names are not implemented\r\n");
			break;

		default:
			uart_send_str(UART0, "Unexpected error while opening test file\r\n");
			break;
		}
		free(config);
		free(devlist);
		Fat_deinit(&fat);
		mmc_deinit(mmc);
		return;
	}
	FatFile *aleph_fp = aleph_res.result;

	vmem_table1_level1_clear();
	vmem_fill_used_bitfield(512 * 1024);
	vmem_map_ram(0x80000000, 512);

	VMemAttr stack_attr = {
		.executable = 0,
		.writable = 1
	};
	VMemStatus map_st = vmem_map_kernel_space(-2 * 1024 * 1024, 2 * 1024 * 1024, stack_attr);
	if(map_st == VMEM_OUT_OF_MEMORY)
	{
		uart_send_str(UART0, "Out of memory\r\n");
		free(config);
		free(devlist);
		FatFile_close(aleph_fp);
		Fat_deinit(&fat);
		mmc_deinit(mmc);
		return;
	}

	vmem_start();

	ElfLoadResult aleph_load_res = elf_load_kernel(aleph_fp);
	if(aleph_load_res.status != ELF_LOAD_OK)
	{
		switch(aleph_load_res.status)
		{
		case ELF_LOAD_OK:
			break;

		case ELF_LOAD_FORMAT_ERROR:
			uart_send_str(UART0, "Bad kernel file format\r\n");
			break;

		case ELF_LOAD_FAT_ERROR:
			uart_send_str(UART0, "FAT reading error\r\n");
			break;

		case ELF_LOAD_MEM_ERROR:
			uart_send_str(UART0, "Virtual memory error\r\n");
			break;

		case ELF_LOAD_OUT_OF_MEMORY:
			uart_send_str(UART0, "Out of memory\r\n");
			break;
		}
		vmem_stop();
		free(config);
		free(devlist);
		FatFile_close(aleph_fp);
		Fat_deinit(&fat);
		mmc_deinit(mmc);
		return;
	}

	uart_send_str(UART0, "Jumping to the entry point\r\n");
	AlephBootInfo boot_info = {
		.devlist = (char*)devlist,
		.devlist_length = devlist_len,
		.config = (char*)config,
		.config_length = config_len,
		.phys_memory_size = 512 * 1024
	};
	void (*entry_point)(AlephBootInfo*) = (void*)aleph_load_res.entry_point;
	entry_point(&boot_info);
}

void on_irq(void)
{
	u8 current_irq = intc_current_irq();
	switch(current_irq)
	{
	case 28:
		mmc_on_irq(&MMC1);
		break;

	case 64:
		mmc_on_irq(&MMC0);
		break;

	default:
		break;
	}
	intc_next_interrupt();
}

void undefined_instruction(u32 is_thumb)
{
	vmem_stop();
	uart_send_str(UART0, "Undefined instruction!\r\n");
	u32 ifsr;
	asm("mrc p15, 0, %0, c5, c0, 1"
			: "=r" (ifsr)
		);
	uart_send_usize(UART0, ifsr);
	uart_send_str(UART0, "\r\n");
	if(is_thumb)
		uart_send_str(UART0, "From Thumb mode\r\n");
	else
		uart_send_str(UART0, "From ARM mode\r\n");
}

void prefetch_abort(void)
{
	vmem_stop();
	uart_send_str(UART0, "Prefetch abort!\r\n");
}

void data_abort(void)
{
	vmem_stop();
	uart_send_str(UART0, "Data abort!\r\n");
	// vmem_dump_table1();
	u32 dfsr;
	asm("mrc p15, 0, %0, c5, c0, 0"
			: "=r" (dfsr)
		);
	u32 fs = (dfsr & 0b1111) | (dfsr >> 6 & 1 << 4);
	uart_send_usize(UART0, fs);
	uart_send_str(UART0, "\r\n");
	u32 dfar;
	asm("mrc p15, 0, %0, c6, c0, 0"
			: "=r" (dfar)
		);
	uart_send_usize(UART0, dfar);
	uart_send_str(UART0, "\r\n");
	u32 psr;
	asm("mrs %0, spsr" : "=r" (psr));
	uart_send_str(UART0, "psr = ");
	uart_send_usize(UART0, psr);
	uart_send_str(UART0, "\r\n");
}

void dump_user_sp_and_lr(u32 sp, u32 lr)
{
	uart_send_str(UART0, "sp_usr = ");
	uart_send_usize(UART0, sp);
	uart_send_str(UART0, "  lr_usr = ");
	uart_send_usize(UART0, lr);
	uart_send_str(UART0, "\r\n");
}

void dump_previous_pc(u32 pc)
{
	uart_send_str(UART0, "PC = ");
	uart_send_usize(UART0, pc);
	uart_send_str(UART0, "\r\n");
}
