#include "gpio.h"

void gpio_pin_mode(GpioRegs *gpio, u8 pin, GpioPinMode mode)
{
    if(mode)
        gpio[GPIO_OE] |= 1 << pin;
    else
        gpio[GPIO_OE] &= ~(1 << pin);
}

void gpio_pin_write(GpioRegs *gpio, u8 pin, int state)
{
    if(state)
        gpio[GPIO_SETDATAOUT] = 1 << pin;
    else
        gpio[GPIO_CLEARDATAOUT] = 1 << pin;
}
