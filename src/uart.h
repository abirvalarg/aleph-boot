#pragma once
#include "types.h"
#include "string.h"

typedef volatile u32 UartRegs;

void uart_reset(UartRegs *uart);
void uart_setup(UartRegs *uart);
void uart_send_byte(UartRegs *uart, char byte);
void uart_send_str(UartRegs *uart, const char *str);
void uart_send_string(UartRegs *uart, Str str);
void uart_send_u8(UartRegs *uart, u8 value);
void uart_send_usize(UartRegs *uart, u32 value);

#define UART_THR 0
#define UART_DLL 0
#define UART_IER (0x4 / 4)
#define UART_DLH (0x4 / 4)
#define UART_EFR (0x8 / 4)
#define UART_LCR (0xc / 4)
#define UART_MDR1 (0x20 / 4)
#define UART_SSR (0x44 / 4)
#define UART_SYSC (0x54 / 4)
#define UART_SYSS (0x58 / 4)

#define UART_ENHANCEDEN_SHIFT 4
#define UART_ENHANCEDEN_MASK (1 << UART_ENHANCEDEN_SHIFT)

#define UART_MODESELECT_SHIFT 0
#define UART_MODESELECT_MASK (0b111 << UART_MODESELECT_SHIFT)

#define UART_MODE_UART_16X 0
#define UART_MODE_UART_13X 3
#define UART_MODE_DISABLE 7

#define UART_CHAR_LENGTH_SHIFT 0

#define UART_CHAR_LENGTH_8BIT 0b11

#define UART0 ((UartRegs*)0x44E09000)
