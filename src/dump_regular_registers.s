.syntax unified

.section .text.dump_regular_registers
.global dump_regular_registers
dump_regular_registers:
	push {lr}

	bl dump_regs_0_to_3

	mov r0, r4
	mov r1, r5
	mov r2, r6
	mov r3, r7
	bl dump_regs_4_to_7

	mov r0, r8
	mov r1, r9
	mov r2, r10
	mov r3, r11
	bl dump_regs_8_to_11

	mov r0, r12
	bl dump_reg_12

	pop {pc}
