#include "mbr.h"
#include "heap.h"
#include "mmc.h"

MbrStatus Mbr_init(Mbr *mbr, Mmc *mmc)
{
	u8 *buffer = malloc(512);
	if(!buffer)
		return MBR_OUT_OF_MEMORY;
	MmcStatus st = mmc_read_block(mmc, 0, buffer);
	switch(st)
	{
	case MMC_OK:
		break;

	case MMC_PROBLEM_WITH_VOLTAGE:
	case MMC_UNSUPPORTED_DEVICE:
		free(buffer);
		return MBR_MMC_ERROR;

	case MMC_OUT_OF_MEMORY:
		free(buffer);
		return MBR_OUT_OF_MEMORY;

	case MMC_OUT_OF_RANGE:
		free(buffer);
		return MBR_OUT_OF_RANGE;
	}
	if(buffer[510] != 0x55 || buffer[511] != 0xaa)
	{
		free(buffer);
		return MBR_NO_SIGNATURE;
	}
	for(unsigned partition = 0; partition < 4; partition++)
	{
		u8 *partition_info = buffer + 446 + partition * 16;
		mbr->partitions[partition] = (MbrPartitionInfo) {
			.first_sector = partition_info[8] | partition_info[9] << 8 | partition_info[0xa] << 16
				| partition_info[0xb] << 24,
			.num_sectors = partition_info[0xc] | partition_info[0xd] << 8 | partition_info[0xe] << 16
				| partition_info[0xf] << 24,
			.status = partition_info[0],
			.partition_type = partition_info[4]
		};
	}
	free(buffer);
	mbr->mmc = mmc;
	return MBR_OK;
}

MbrPartition Mbr_partition(const Mbr *mbr, u8 partition)
{
	partition = partition % 4;
	return (MbrPartition) {
		.mmc = mbr->mmc,
		.first_sector = mbr->partitions[partition].first_sector,
		.num_sectors = mbr->partitions[partition].num_sectors
	};
}

MbrStatus MbrPartition_read_sector(MbrPartition part, u32 sector, u8 *buffer)
{
	MmcStatus st = mmc_read_block(part.mmc, part.first_sector + sector, buffer);
	if(st == MMC_OK)
		return MBR_OK;
	else if(st == MMC_OUT_OF_RANGE)
		return MBR_OUT_OF_RANGE;
	else
		return MBR_MMC_ERROR;
}
