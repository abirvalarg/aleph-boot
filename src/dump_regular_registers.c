#include "types.h"
#include "uart.h"

void dump_regs_0_to_3(u32 r0, u32 r1, u32 r2, u32 r3)
{
	uart_send_str(UART0, "r0 = ");
	uart_send_usize(UART0, r0);
	uart_send_str(UART0, "  r1 = ");
	uart_send_usize(UART0, r1);
	uart_send_str(UART0, "  r2 = ");
	uart_send_usize(UART0, r2);
	uart_send_str(UART0, "  r3 = ");
	uart_send_usize(UART0, r3);
	uart_send_str(UART0, "\r\n");
}

void dump_regs_4_to_7(u32 r4, u32 r5, u32 r6, u32 r7)
{
	uart_send_str(UART0, "r4 = ");
	uart_send_usize(UART0, r4);
	uart_send_str(UART0, "  r5 = ");
	uart_send_usize(UART0, r5);
	uart_send_str(UART0, "  r6 = ");
	uart_send_usize(UART0, r6);
	uart_send_str(UART0, "  r7 = ");
	uart_send_usize(UART0, r7);
	uart_send_str(UART0, "\r\n");
}

void dump_regs_8_to_11(u32 r8, u32 r9, u32 r10, u32 r11)
{
	uart_send_str(UART0, "r8 = ");
	uart_send_usize(UART0, r8);
	uart_send_str(UART0, "  r9 = ");
	uart_send_usize(UART0, r9);
	uart_send_str(UART0, "  r10 = ");
	uart_send_usize(UART0, r10);
	uart_send_str(UART0, "  r11 = ");
	uart_send_usize(UART0, r11);
	uart_send_str(UART0, "\r\n");
}

void dump_reg_12(u32 r12)
{
	uart_send_str(UART0, "r12 = ");
	uart_send_usize(UART0, r12);
	uart_send_str(UART0, "\r\n");
}
