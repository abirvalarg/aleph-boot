#pragma once
#include "types.h"

typedef volatile u32 EmifRegs;

void emif_config_ddr_phy(void);
void emif_set_timings(void);
void emif_config_sdram(void);

#define SDRAM_CONFIG (0x8 / 4)
#define SDRAM_REF_CTRL (0x10 / 4)
#define SDRAM_REF_CTRL_SHDW (0x14 / 4)
#define SDRAM_TIM_1 (0x18 / 4)
#define SDRAM_TIM_1_SHDW (0x1c / 4)
#define SDRAM_TIM_2 (0x20 / 4)
#define SDRAM_TIM_2_SHDW (0x24 / 4)
#define SDRAM_TIM_3 (0x28 / 4)
#define SDRAM_TIM_3_SHDW (0x2c / 4)
#define ZQ_CONFIG (0xc8 / 4)
#define DDR_PHY_CTRL_1 (0xe4 / 4)
#define DDR_PHY_CTRL_1_SHDW (0xe8 / 4)

#define EMIF0 ((EmifRegs*)0x4c000000)
