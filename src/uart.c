#include "uart.h"
#include "exclusive.h"
#include "../config.h"

void uart_reset(UartRegs *base)
{
    base[UART_SYSC] |= 2;
    while(base[UART_SYSS] == 0);
}

void uart_setup(UartRegs *uart)
{
    uart[UART_MDR1] = UART_MODE_DISABLE << UART_MODESELECT_SHIFT;
    uart[UART_LCR] = 0;
    uart[UART_IER] = 0;
    uart[UART_LCR] = 0xbf;
    uart[UART_DLL] = UART_DIVISOR & 0xff;
    uart[UART_DLH] = UART_DIVISOR >> 8;
    uart[UART_LCR] = UART_CHAR_LENGTH_8BIT << UART_CHAR_LENGTH_SHIFT;
#if UART_OVERSAMPLING == 16
    uart[UART_MDR1] = UART_MODE_UART_16X << UART_MODESELECT_SHIFT;
#elif UART_OVERSAMPLING == 13
    uart[UART_MDR1] = UART_MODE_UART_13X << UART_MODESELECT_SHIFT;
#else
#error "Unsupported oversampling mode"
#endif
}

void uart_send_byte(UartRegs *uart, char byte)
{
    while(uart[UART_SSR] & 1);
    uart[UART_THR] = byte;
}

void uart_send_str(UartRegs *uart, const char *str)
{
    for(usize i = 0; str[i]; i++)
        uart_send_byte(uart, str[i]);
}

void uart_send_string(UartRegs *uart, Str str)
{
	for(unsigned i = 0; i < str.length; i++)
		uart_send_byte(uart, str.pointer[i]);
}

void uart_send_u8(UartRegs *uart, u8 value)
{
	u8 digit1_value = value >> 4;
	if(digit1_value > 9)
		digit1_value += 'a' - 10;
	else
		digit1_value += '0';
	uart_send_byte(uart, digit1_value);
	u8 digit2_value = value & 0xf;
	if(digit2_value > 9)
		digit2_value += 'a' - 10;
	else
		digit2_value += '0';
	uart_send_byte(uart, digit2_value);
}

void uart_send_usize(UartRegs *uart, u32 value)
{
	char buffer[8];
	for(int pos = 7; pos >= 0; pos--)
	{
		u32 digit_value = value & 0xf;
		buffer[pos] = digit_value > 9 ? digit_value - 10 + 'a' : digit_value + '0';
		value >>= 4;
	}
	for(int pos = 0; pos < 8; pos++)
		uart_send_byte(uart, buffer[pos]);
}
