#pragma once
#include <stddef.h>
#include "types.h"

void *memset(void *dest, int ch, size_t count);
void *memcpy(void *restrict _dest, const void *restrict _src, size_t count);

int memory_equal(u8 *a, u8 *b, u32 length);
