#include "elf.h"
#include "fat.h"
#include "heap.h"
#include "vmem.h"

#include "uart.h"

typedef struct ElfIdent
{
	u8 magic[4];
	u8 class;
	u8 data_encoding;
	u8 version;
	u8 padding[8];
	u8 nident;
} ElfIdent;

typedef struct ElfHeader
{
	ElfIdent ident;
	u16 type;
	u16 machine;
	u32 version;
	u32 entry;
	u32 program_offset;
	u32 section_offset;
	u32 flags;
	u16 ehsize;
	u16 program_header_entry_size;
	u16 program_header_entry_number;
	u16 shentsize;
	u16 shnum;
	u16 shstrndx;
} ElfHeader;

typedef struct ElfHeaderInfo
{
	u32 entry_point;
	u32 program_header_start;
	u16 program_header_entry_size;
	u16 program_header_entry_number;
	ElfLoadStatus status;
} ElfHeaderInfo;

typedef struct ProgramHeaderEntry
{
	u32 type;
	u32 offset;
	u32 vaddr;
	u32 paddr;
	u32 file_size;
	u32 mem_size;
	u32 flags;
	u32 align;
} ProgramHeaderEntry;

static ElfHeaderInfo decode_header(FatFile *fp, u32 *pos_in_file);

ElfLoadResult elf_load_kernel(FatFile *fp)
{
	u32 pos_in_file = 0;
	ElfHeaderInfo header_info = decode_header(fp, &pos_in_file);
	if(header_info.status != ELF_LOAD_OK)
		return (ElfLoadResult) { .status = header_info.status };

	if(header_info.program_header_entry_size != sizeof(ProgramHeaderEntry))
		return (ElfLoadResult) { .status = ELF_LOAD_FORMAT_ERROR };

	while(pos_in_file < header_info.program_header_start)
	{
		u8 dummy;
		FatFileReadResult res = FatFile_read(fp, &dummy, 1);
		if(res.status != FAT_OK)
			return (ElfLoadResult) { .status = ELF_LOAD_FAT_ERROR };
		if(res.length == 0)
			return (ElfLoadResult) { .status = ELF_LOAD_FORMAT_ERROR };
		pos_in_file += res.length;
	}

	if(pos_in_file > header_info.program_header_start)
		return (ElfLoadResult) { .status = ELF_LOAD_FORMAT_ERROR };

	u32 bytes_in_program_header =
		sizeof(ProgramHeaderEntry) * header_info.program_header_entry_number;

	ProgramHeaderEntry *program_header =
		malloc(bytes_in_program_header);

	{
		u32 bytes_read = 0;
		do
		{
			FatFileReadResult res =
				FatFile_read(
						fp,
						(u8*)program_header + bytes_read,
						bytes_in_program_header - bytes_read
					);
		if(res.status != FAT_OK)
		{
			free(program_header);
			return (ElfLoadResult) { .status = ELF_LOAD_FAT_ERROR };
		}
		if(res.length == 0)
		{
			free(program_header);
			return (ElfLoadResult) { .status = ELF_LOAD_FORMAT_ERROR };
		}
		pos_in_file += res.length;
		bytes_read += res.length;
		} while(bytes_read < bytes_in_program_header);
	}

	for(u32 pos = 0; pos < header_info.program_header_entry_number; pos++)
	{
		if(program_header[pos].offset && program_header[pos].offset < pos_in_file)
		{
			free(program_header);
			return (ElfLoadResult) { .status = ELF_LOAD_FORMAT_ERROR };
		}

		VMemAttr attr = {
			.executable = 0,
			.writable = 1
		};
		VMemStatus map_st = vmem_map_kernel_space(program_header[pos].vaddr, program_header[pos].mem_size, attr);
		if(map_st != VMEM_OK)
		{
			free(program_header);
			return (ElfLoadResult) { .status = ELF_LOAD_MEM_ERROR };
		}
		while(program_header[pos].offset > pos_in_file)
		{
			u8 dummy;
			FatFileReadResult read_res = FatFile_read(fp, &dummy, 1);
			if(read_res.status != FAT_OK)
			{
				free(program_header);
				return (ElfLoadResult) { .status = ELF_LOAD_FAT_ERROR };
			}
			if(read_res.length != 1)
			{
				free(program_header);
				return (ElfLoadResult) { .status = ELF_LOAD_FORMAT_ERROR };
			}
			pos_in_file++;
		}

		u32 bytes_read = 0;
		while(bytes_read < program_header[pos].file_size)
		{
			FatFileReadResult read_res = FatFile_read(
					fp,
					(u8*)(program_header[pos].vaddr + bytes_read),
					program_header[pos].file_size - bytes_read
				);
			if(read_res.status != FAT_OK)
			{
				free(program_header);
				return (ElfLoadResult) { .status = ELF_LOAD_FAT_ERROR };
			}
			if(read_res.length == 0)
			{
				free(program_header);
				return (ElfLoadResult) { .status = ELF_LOAD_FORMAT_ERROR };
			}
			bytes_read += read_res.length;
			pos_in_file += read_res.length;
		}
		while(bytes_read < program_header[pos].mem_size)
		{
			*(u8*)(program_header[pos].vaddr + bytes_read) = 0;
			bytes_read++;
		}

		attr = (VMemAttr) {
			.executable = program_header[pos].flags & 1,
			.writable = (program_header[pos].flags & 2) >> 1
		};
		vmem_change_kernel_space_attr(program_header[pos].vaddr, program_header[pos].mem_size, attr);
		// invalidate instruction cache
		asm("mcr p15, 0, %0, c7, c5, 0"
				:
				: "r" (1)
			);
		// invalidate branch predictor
		asm("mcr p15, 0, %0, c7, c5, 6"
				:
				: "r" (1)
			);
		asm("isb");
	}

	free(program_header);
	return (ElfLoadResult) {
		.status = ELF_LOAD_OK,
		.entry_point = header_info.entry_point
	};
}

static ElfHeaderInfo decode_header(FatFile *fp, u32 *pos_in_file)
{
	ElfHeader header;
	u32 bytes_read = 0;
	do
	{
		FatFileReadResult read_res = FatFile_read(
				fp,
				((u8*)&header) + bytes_read,
				sizeof(ElfHeader) - bytes_read
		);
		if(read_res.status != FAT_OK)
			return (ElfHeaderInfo) { .status = ELF_LOAD_FAT_ERROR };
		if(read_res.length == 0)
			return (ElfHeaderInfo) { .status = ELF_LOAD_FORMAT_ERROR };
		bytes_read += read_res.length;
		*pos_in_file += read_res.length;
	} while(bytes_read < sizeof(ElfHeader));

	if(!(header.ident.magic[0] == 0x7f
				&& header.ident.magic[1] == 'E'
				&& header.ident.magic[2] == 'L'
				&& header.ident.magic[3] == 'F'))
		return (ElfHeaderInfo) { .status = ELF_LOAD_FORMAT_ERROR };

	if(header.ident.class != 1)	// 32 bit
		return (ElfHeaderInfo) { .status = ELF_LOAD_FORMAT_ERROR };

	if(header.ident.data_encoding != 1)	// LSB
		return (ElfHeaderInfo) { .status = ELF_LOAD_FORMAT_ERROR };

	if(header.type != 2)	// executable
		return (ElfHeaderInfo) { .status = ELF_LOAD_FORMAT_ERROR };

	if(header.machine != 40)	// ARM/Thumb
		return (ElfHeaderInfo) { .status = ELF_LOAD_FORMAT_ERROR };

	if(header.version != 1)	// current version
		return (ElfHeaderInfo) { .status = ELF_LOAD_FORMAT_ERROR };

	return (ElfHeaderInfo) {
		.entry_point = header.entry,
		.program_header_start = header.program_offset,
		.program_header_entry_size = header.program_header_entry_size,
		.program_header_entry_number = header.program_header_entry_number,
		.status = ELF_LOAD_OK
	};
}
