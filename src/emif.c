#include "emif.h"
#include "control.h"

void emif_config_ddr_phy(void)
{
    EMIF0[DDR_PHY_CTRL_1] = 0x100007;
    EMIF0[DDR_PHY_CTRL_1_SHDW] = 0x100007;
}

void emif_set_timings(void)
{
    EMIF0[SDRAM_TIM_1] = 0x0aaad4db;
    EMIF0[SDRAM_TIM_1_SHDW] = 0x0aaad4db;
    EMIF0[SDRAM_TIM_2] = 0x266b7fda;
    EMIF0[SDRAM_TIM_2_SHDW] = 0x266b7fda;
    EMIF0[SDRAM_TIM_3] = 0x501f867f;
    EMIF0[SDRAM_TIM_3_SHDW] = 0x501f867f;
}

void emif_config_sdram(void)
{
    EMIF0[SDRAM_REF_CTRL] = 0x2800;
    EMIF0[ZQ_CONFIG] = 0x50074be4;
    CONTROL[CONTROL_EMIF_SDRAM_CONFIG] = 0x61c05332;
    EMIF0[SDRAM_CONFIG] = 0x61c05332;
    EMIF0[SDRAM_REF_CTRL] = 0xc30;
    EMIF0[SDRAM_REF_CTRL_SHDW] = 0xc30;
}
