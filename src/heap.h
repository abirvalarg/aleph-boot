#pragma once
#include "types.h"

void heap_init(void);
void *malloc(u32 size);
void free(void *ptr);
