CROSS_COMPILE=arm-none-eabi-

# Compiler for tools
TCC=gcc

# cross-compile tollchains
CAS=$(CROSS_COMPILE)as
CCC=$(CROSS_COMPILE)gcc
CLD=$(CROSS_COMPILE)ld
COBJCOPY=$(CROSS_COMPILE)objcopy

AFLAGS=
CFLAGS=-c -O2 -ffunction-sections -fdata-sections -fno-builtin -ffreestanding \
	   -fno-tree-loop-distribute-patterns -Wall -Wextra -std=gnu2x -g -march=armv7 \
	   -mno-unaligned-access
LFLAGS=-gc-sections -g

CSRC=$(wildcard src/*.c)
ASRC=$(wildcard src/*.s)
SRC=$(CSRC) $(ASRC)
OBJ=$(SRC:src/%=build/obj/%.o)
DEPS=$(CSRC:src/%=build/deps/%.deps)

install/MLO: build/boot.bin build/tools/make-mlo
	@mkdir -p $(@D)
	build/tools/make-mlo $< $@

build/boot.bin: build/boot
	@mkdir -p $(@D)
	$(COBJCOPY) -O binary $< $@

build/boot: $(OBJ) map.ld
	@mkdir -p $(@D)
	$(CLD) $(LFLAGS) -T map.ld -o $@ $(OBJ) $(shell $(CCC) -print-libgcc-file-name)

build/obj/%.c.o: src/%.c Makefile
	@mkdir -p $(@D)
	$(CCC) $(CFLAGS) -o $@ $<

build/obj/%.s.o: src/%.s Makefile
	@mkdir -p $(@D)
	$(CAS) $(AFLAGS) -o $@ $<

build/deps/%.deps: src/% Makefile
	@mkdir -p $(@D)
	$(CCC) -M $< | sed 's/\(.*\)\.o:/build\/obj\/\1\.c\.o:/' > $@

build/tools/%: tools/%.c Makefile
	@mkdir -p $(@D)
	$(TCC) -o $@ $<

include $(DEPS)

clean:
	rm -r build install 2> /dev/null || true

.PHONY: clean
