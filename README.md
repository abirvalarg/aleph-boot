# aleph-boot
Bootloader for aleph OS

# Boot interface
Lhe bootloader loads an ELF file from `/boot/aleph` info memory starting at 0xa0000000 (2GiB + 512MiB),
defines a 2MiB stack at the very top of the memory space, sets r0 to a pointer to the following structure
with boot information and jumps to the entry point defined in the ELF header.

```c
typedef struct AlephBootInfo
{
    const char *devlist;    // Pointer to the devlist string in bootloader memory space
    uint32_t devlist_length;    // Length of the devlist in bytes
    const char *config;         // Pointer to the config string in bootloader memory space
    uint32_t config_length;     // Length of the config in bytes
    uint32_t phys_memory_size;  // Size of physical memory in KiBs
} AlephBootInfo;
```

## Reserved memory region
Bootloader initializes a reserved region in the beginning of the memory. 1st 16KiB are taken by the
kernel space level 1 translation table, next is the bitfield where every KiB of physical memory is
represented with a single bit. The bitfield includes reserved region. 0 means that the KiB is free,
1 - that KiB is in use. For example, for memory size of 512MiB, the bitfield will take 64KiB and total
size of the reserved region is 80KiB.
